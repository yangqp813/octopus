module testsuite_sort_oct_m
  use fortuno_interface_m
  use sort_oct_m

  implicit none
  private
  public :: testsuite_sort

contains

  !> Returns a suite instance, wrapped as test_item
  function testsuite_sort() result(res)
    type(test_item), allocatable :: res

    res = test_suite("basic/sort", &
      items = [&
        test_case("test_sort", test_sort)&
      ]&
      )
  end function testsuite_sort

  subroutine test_sort()
    integer :: list(3)

    list = [2, 1, 3]
    call sort(list)
    call check(all(list == [1, 2, 3]))
  end subroutine test_sort

end module testsuite_sort_oct_m
