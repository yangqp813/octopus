!> @brief Register tests by providing a test suite instance containing all tests associated
!!  with an Octopus module.
!!
!! For example:
!! ```
!! call execute_app(testitems = [testsuite_oct_module, testsuite_oct_module2])
!! ```
!! where a test_suite instance contains the module name, and an array of test_case
!! instances for routines in that module (one test_case per unit test):
!! ```
!! testsuite_sort = test_suite("basic/sort", &
!!                             items = [test_case("test_sort", test_sort)], &
!!                            )
!!```
!! Note: this routine does not return but stops the program with the right exit code.
program test_suite
  use fortuno_interface_m, only : execute_cmd_app
  use testsuite_sort_oct_m
  implicit none

  call execute_cmd_app(&
          testitems=[&
                  testsuite_sort()&
                  ]&
          )

end program test_suite
