# Octopus Unit Testing with Fortuno

<!-- TOC -->

- [Introduction](#introduction)
- [Registering a Unit Test](#registering-a-unit-test)
- [Writing a Serial Unit Test](#writing-a-serial-unit-test)
- [Writing an MPI Unit Test](#writing-an-mpi-unit-test)

<!-- TOC -->

## Introduction

Octopus unit testing is based on the [Fortuno](https://github.com/fortuno-repos/fortuno) test framework, which is 
automatically fetched and installed by cmake at build time. Note that Fortuno requires gfortran >= 12.0.0 to be
compiled.

Unit tests can be run via `ctest`:

```shell
# List all available tests (including regression)
ctest --test-dir ./build -N
# Run all unit tests via labels
ctest --test-dir ./build ctest -L unit_tests
# Execute the specific test `basic/sort`
ctest --test-dir ./build -R basic/sort
```

where `./build` should be replaced with the build directory name.`ctest` will not write to stdout by default. 
This can be changed with the `--verbose` flag. 

The unit tests can equivalently be run by directly executing the test binary:

```shell
# List available tests 
./build/test/test-suite --list
# Execute the test `basic/sort`
./build/test/test-suite basic/sort
```

where `--list` lists all registered tests. 

To run a test, the corresponding subroutine must be registered with Fortuno via the `test_suite` function, in the 
respective test module. More on this in [Registering a Unit Test](#registering-a-unit-test). If the test file is new, it should also be added 
to the `CMakeLists.txt`, in its respective folder:

```cmake
target_sources(Octopus_test_suite PRIVATE
    test_module.f90     # New test module, defined in test/subfolder/test_module.f90
)
foreach (test IN ITEMS
    <subfolder/module>  # Test name, as defined in the test_suite("subfolder/module", items) registration of test_module.f90
)
```

We also note that Octopus has an existing unit test driver file, `src/main/test.F90`, in which many tests are already
present. This requires specification in an Octopus input file to be executed. Adding tests to `test.F90` should be 
considered deprecated in favour of using Fortuno.


## Test Structure and Registering a Unit Test

The structure of `test` mirrors that of the `src`. Each subfolder of the `src` should have the same named folder in
`test`. Within this folder, one should find and tests for each module, following the convention `test_<module-name>.f90`. 
For example:

```console
$ tree ./
./
├── src
│   └── basic
│       └── sort.F90        # Octopus module
└── test
    ├── basic
    │   └── test_sort.F90   # Module to test routines in src/basic/sort.f90
    └── fortuno_app.F90     # Collate subroutine drivers from each registration file in the subfolders of test/ 
```

A test module should always contain the boilerplate:

```fortran
module testsuite_SUBFOLDER_oct_m
  use fortuno_interface_m
  use my_oct_m    ! Module containing routines to test
  implicit none

  private
  public :: testsuite_SUBFOLDER
    
contains

    !> Returns a suite instance, wrapped as test_item
    function testsuite_SUBFOLDER() result(res)
    type(test_item), allocatable :: res
    res = test_suite("SUBFOLDER/MODULE", &
            items = [test_case("test_name", test_subroutine)]&  ! TODO Add test cases to items array
            )
  end function testsuite_SUBFOLDER
    
  subroutine test_subroutine()
    ! Test a routine from my_oct_m   
  end subroutine test_subroutine
    
end module testsuite_SUBFOLDER_oct_m
```

where `SUBFOLDER` and `MODULE` are replaced with the appropriate names. If this is new file, one is also required to add the 
`use testsuite_SUBFOLDER_oct_m` statement and the test driver function `testsuite_SUBFOLDER` routine to 
the `test/fortuno_app.f90`:

```fortran
program test_suite
  use fortuno_interface_m, only : execute_cmd_app
  use test_basic_oct_m
  use testsuite_SUBFOLDER_oct_m  ! TODO. Makes the new test driver available
  implicit none
  
  call execute_app(&
          testitems=[&
                  testsuite_basic(), &
                  testsuite_SUBFOLDER(), &  ! TODO. Registers a new test driver for SUBFOLDER
                  ]&
          )
end program test_suite

```

## Writing a Serial Unit Test
 
TODO. Provide some details on:

* setups and teardowns
* Writing a test

## Writing an MPI Unit Test

TODO. Provide some details on:

* setups and teardowns
* Writing a test

## Limitations

Many Octopus classes contain a call to access a single instance of the parsed Octopus `inp`ut file. In the absence of
an input file, this code will fail. This can be circumvented by defining an `inp` string in the test and have the Octopus
parser parse the string as a test setup routine. 
