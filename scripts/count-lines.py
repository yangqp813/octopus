#!/usr/bin/python3

import os
import stat
import time
import shutil
import subprocess


start_year  = 2002
start_month = 1

mytime = time.localtime()
current_month = mytime[1]
current_year = mytime[0]

lines_filename='lines.dat'
OUT = open(lines_filename, 'w')

tics = ""
i = 0
year = start_year
month = start_month

while True:
    ghash = subprocess.check_output('git rev-list -n 1 --before="{0}-{1}-01 00:00" main'.format(year, '{0:02d}'.format(month)), shell=True).decode('utf-8').strip()
    subprocess.call('git checkout {0}'.format(ghash), shell=True)

    lf90 = 0; lc = 0; lcl = 0; lcc = 0
    for subdir, dirs, files in os.walk("."):
        if 'external_libs' in subdir:
            continue
        for file in files:
            if file.endswith('.F90'):
                lf90 += sum(1 for line in open(os.path.join(subdir, file), errors='ignore'))
            if file.endswith('.c') or file.endswith('.h') and file != 'config.h':
                lc += sum(1 for line in open(os.path.join(subdir, file), errors='ignore'))
            if file.endswith('.cl'):
                lcl += sum(1 for line in open(os.path.join(subdir, file), errors='ignore'))
            if file.endswith('.cc'):
                lcc += sum(1 for line in open(os.path.join(subdir, file), errors='ignore'))

    OUT.write('{0:6d} {1:2d} {2:4d} {3:7d} {4:7d} {5:7d} {6:7d}\n'.format(i, month, year, lf90, lc, lcl, lcc))

    if i == 0 or i%12 == 0:
        if i != 0:
            tics = tics + ","
        tics = "{0}'{1}' {2}".format(tics, str(year)[2:5], i)

    if year == current_year and month == current_month:
        break

    i += 1
    month += 1
    if month == 13:
        month = 1
        year += 1

OUT.close()

gp_filename='lines.gp'
GP = open(gp_filename, 'w')
GP.write('set key left\n')
GP.write("set xlabel 'Time'\n")
GP.write("set ylabel 'kilo Lines of code'\n")
GP.write("set xtics({0})\n".format(tics))
GP.write("set term post eps color solid 22\n")
GP.write("set out 'oct_lines.eps'\n")
GP.write("pl '"+lines_filename+"' u 1:($4/1000) t 'F90 lines' w l lw 3, '' u 1:($5/1000) t 'C lines' w l lt 3 lw 3, '' u 1:($6/1000) t 'OpenCL lines' w l lt 5 lw 3, '' u 1:($7/1000) t 'C++ lines' w l lt 2 lw 3,'' u 1:(($4+$5+$6+$7)/1000) t 'Total lines' w l lt 4 lw 4\n")
GP.close()

subprocess.call(['gnuplot', gp_filename])
