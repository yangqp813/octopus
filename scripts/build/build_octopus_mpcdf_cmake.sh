#!/bin/bash
set -e

_arg_config="on"
_arg_compiler="intel"
_arg_mpi="impi"
_arg_cuda="off"

die()
{
    _die_ret="${2:-1}"
    test "${_PRINT_HELP:-no}" = yes && print_help >&2
    echo "$1" >&2
    exit "${_die_ret}"
}

print_help()
{
    printf '%s\n' "Compilation script for octopus using cmake."
    print_usage
}

print_usage()
{
    printf '\nUsage: %s [--buildtype <arg>] [-h|--help]\n' "$0"
    printf '\t%s\n' "--config, --no-config: run configure step (on by default)"
    printf '\t%s\n' "--compiler: compiler type. available options: 'intel' or 'gcc' (default: 'intel')"
    printf '\t%s\n' "--mpi: MPI implementation. available options: 'impi', or 'openmpi' (default: 'impi')"
    printf '\t%s\n' "--cuda, --no-cuda: enable CUDA (default: 'off')"
    printf '\t%s\n' "-h, --help: print help (this list!)"
}

parse_commandline()
{
    while test $# -gt 0
    do
        _key="$1"
        case "$_key" in
            --no-config|--config)
                test "${1:0:5}" = "--no-" && _arg_config="off"
                ;;
            --compiler)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_compiler="$2"
                shift
                ;;
            --mpi)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_mpi="$2"
                shift
                ;;
            --no-cuda|--cuda)
                test "${1:0:6}" = "--cuda" && _arg_cuda="on"
                ;;
            -h|--help)
                print_help
                exit 0
                ;;
            -h*)
                print_help
                exit 0
                ;;
            *)
                _PRINT_HELP=yes die "FATAL ERROR: Unexpected argument '$1'" 1
                ;;
        esac
        shift
    done
}

parse_commandline "$@"

case $_arg_compiler in
  "intel")
    build_modules="intel/2024.0"
    ;;
  "gcc")
    build_modules="gcc/11"
    ;;
  *)
    echo "Error, compiler $_arg_compiler not supported"
    ;;
esac
if [[ "$_arg_cuda" == "on" ]]; then
  build_modules="$build_modules cuda/11.6"
  CUDA="-DOCTOPUS_CUDA:BOOL=ON"
fi
case $_arg_mpi in
  "impi")
    build_modules="$build_modules impi/2021.11"
    ;;
  "openmpi")
    if [[ "$_arg_cuda" == "on" ]]; then
      build_modules="$build_modules openmpi_gpu/4.1"
    else
      build_modules="$build_modules openmpi/4.1"
    fi
    ;;
  *)
    echo "Error, compiler $_arg_compiler not supported"
    ;;
esac

module purge
module load cmake ninja
module load $build_modules
module load mkl/2024.0 gsl hdf5-serial netcdf-serial libxc/5.1.5 libvdwxc-mpi cgal boost/1.79 metis-64 parmetis-64 elpa/mpi/openmp/2023.11.001
if [[ "$_arg_compiler" == "intel" ]]; then
  # need a modern gcc for compiling C++ code with the intel compiler
  module load gcc/13
fi
module list


# include compiler and MPI name in build and install directories
build=mpcdf-$_arg_compiler-$_arg_mpi
if [[ "$_arg_cuda" == "on" ]]; then
  build=$build-cuda
fi
if [ "$_arg_config" != "off" ]; then
  cmake -B build-$build --preset mpcdf-$_arg_compiler -DCMAKE_INSTALL_PREFIX=install-$build $CUDA
fi
cmake --build build-$build
cmake --install build-$build
#ctest --test-dir build-$build  -L short-run
