# !1888 TODO(Cristian): To be removed and switched to submodules

add_library(bpdn STATIC
		bpdn/expmm.c
		bpdn/heap.c
		bpdn/oneProjectorCore.c
		bpdn/spgl1_projector.c
		bpdn/bpdn.f90)
target_include_directories(bpdn PRIVATE
		${CMAKE_CURRENT_BINARY_DIR}/../src/include)
target_include_directories(bpdn INTERFACE
		${CMAKE_CURRENT_BINARY_DIR})
add_library(dftd3 STATIC
		dftd3/api.f90
		dftd3/common.f90
		dftd3/core.f90
		dftd3/pars.f90
		dftd3/sizes.f90)
target_include_directories(dftd3 PRIVATE
		${CMAKE_CURRENT_BINARY_DIR}/../src/include)
target_include_directories(dftd3 INTERFACE
		${CMAKE_CURRENT_BINARY_DIR})
file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/dftd3/pars.dat DESTINATION ${PROJECT_BINARY_DIR}/share/dftd3/
		REGEX "(CMakeLists.txt|\\.am$)" EXCLUDE)
add_library(qshep STATIC
		qshep/qshep2d.f90
		qshep/qshep3d.f90)
target_include_directories(qshep PRIVATE
		${CMAKE_CURRENT_BINARY_DIR}/../src/include)
target_include_directories(qshep INTERFACE
		${CMAKE_CURRENT_BINARY_DIR})

add_library(rapidxml INTERFACE)
target_include_directories(rapidxml INTERFACE rapidxml)

if(OCTOPUS_OpenCL)
	add_library(fortrancl STATIC
		fortrancl/cl_types.f90
		fortrancl/cl_buffer.f90
		fortrancl/cl_command_queue.f90
		fortrancl/cl_constants.f90
		fortrancl/cl_context.f90
		fortrancl/cl_device.f90
		fortrancl/cl_kernel.f90
		fortrancl/cl_platform.f90
		fortrancl/cl_program.f90
		fortrancl/cl_event.f90
		fortrancl/cl.f90
		fortrancl/clfft.f90
		fortrancl/utils.c
		fortrancl/cl_buffer_low.c
		fortrancl/cl_command_queue_low.c
		fortrancl/cl_context_low.c
		fortrancl/cl_device_low.c
		fortrancl/cl_kernel_low.c
		fortrancl/cl_platform_low.c
		fortrancl/cl_program_low.c
		fortrancl/cl_event_low.c
		fortrancl/clfft_low.c)
	target_include_directories(fortrancl PRIVATE
		${CMAKE_CURRENT_BINARY_DIR}/../src/include
		fortrancl)
	target_link_libraries(fortrancl PUBLIC
		OpenCL::OpenCL clFFT)
	target_compile_options(fortrancl PRIVATE
		$<$<COMPILE_LANGUAGE:Fortran>:-fallow-argument-mismatch>)

endif()
