!! Copyright (C) 2002-2006 M. Marques, A. Castro, A. Rubio, G. Bertsch
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!



! ---------------------------------------------------------
! These are the workhorse routines that handle the calculation of derivatives
!
! Note that the function whose derivative is to be calculated
! *has* to be defined (1:mesh%np_part), while the (1:mesh%np) values of the derivative
! are calculated.
! ---------------------------------------------------------
!> @brief apply a nl_operator to a mesh function
!!
!! @note the initial mesh function needs to be defined including ghost and boundary points!
!! As the ghost points are updated, the initial mesh function needs to be declared intent(inout).
!
subroutine X(derivatives_perform)(op, der, ff, op_ff, ghost_update, set_bc, factor)
  type(nl_operator_t), target, intent(in)    :: op           !< operator to apply
  type(derivatives_t),         intent(in)    :: der          !< the derivatives object
  R_TYPE, contiguous,          intent(inout) :: ff(:)        !< initial mesh function (1:der%mesh%np_part)
  R_TYPE, contiguous,          intent(out)   :: op_ff(:)     !< final mesh function (1:der%mesh%np)
  logical, optional,           intent(in)    :: ghost_update !< flag whether to perform the ghost update (default .true.)
  logical, optional,           intent(in)    :: set_bc       !< flag whether to apply the boundary conditions (default .true.)
  real(real64),   optional,           intent(in)    :: factor       !< optional scaling factor

  type(batch_t) :: batch_ff, batch_op_ff

  PUSH_SUB(X(derivatives_perform))

  ASSERT(ubound(ff, DIM=1) >= der%mesh%np_part)

  call batch_init(batch_ff, ff)
  call batch_init(batch_op_ff, op_ff)

  call X(derivatives_batch_perform) (op, der, batch_ff, batch_op_ff, ghost_update, set_bc, factor)

  call batch_ff%end()
  call batch_op_ff%end()

  POP_SUB(X(derivatives_perform))

end subroutine X(derivatives_perform)


! ---------------------------------------------------------
! Now the simplified interfaces


! ---------------------------------------------------------
!> @brief apply the Laplacian to a mesh function
!!
!! @note the initial mesh function needs to be defined including ghost and boundary points!
!! As the ghost points are updated, the initial mesh function needs to be declared intent(inout).
!
subroutine X(derivatives_lapl)(der, ff, op_ff, ghost_update, set_bc, factor)
  type(derivatives_t),       intent(in)    :: der          !< derivatives object
  R_TYPE, contiguous,        intent(inout) :: ff(:)        !< initial mesh function (1:der%mesh%np_part)
  R_TYPE, contiguous,        intent(out)   :: op_ff(:)     !< final mesh function (1:der%mesh%np)
  logical, optional,         intent(in)    :: ghost_update !< flag whether to perform the ghost update (default .true.)
  logical, optional,         intent(in)    :: set_bc       !< flag whether to apply the boundary conditions (default .true.)
  real(real64),   optional,         intent(in)    :: factor       !< optional scaling factor

  PUSH_SUB(X(derivatives_lapl))

  call X(derivatives_perform)(der%lapl, der, ff, op_ff, ghost_update, set_bc, factor)

  POP_SUB(X(derivatives_lapl))
end subroutine X(derivatives_lapl)


! ---------------------------------------------------------
!> @brief apply the gradient to a mesh function
!!
!! @note the initial mesh function needs to be defined including ghost and boundary points!
!! As the ghost points are updated, the initial mesh function needs to be declared intent(inout).
!
subroutine X(derivatives_grad)(der, ff, op_ff, ghost_update, set_bc, to_cartesian)
  type(derivatives_t), intent(in)    :: der          !< derivatives object
  R_TYPE, contiguous,  intent(inout) :: ff(:)        !< initial mesh function ff(1:der%mesh%np_part)
  R_TYPE, contiguous,  intent(out)   :: op_ff(:, :)  !< final mesh function op_ff(1:der%mesh%np, 1:der%dim)
  logical, optional,   intent(in)    :: ghost_update !< flag whether to perform the ghost update (default .true.)
  logical, optional,   intent(in)    :: set_bc       !< flag whether to apply the boundary conditions (default .true.)
  logical, optional,   intent(in)    :: to_cartesian !< flag whether conversion to cartesian coords in needed

  integer :: idir,ip
  logical :: set_bc_, ghost_update_

  PUSH_SUB(X(derivatives_grad))
  call profiling_in(TOSTRING(X(GRADIENT)))

  ASSERT(ubound(op_ff, DIM=2) >= der%dim)

  set_bc_ = optional_default(set_bc, .true.)
  ghost_update_ = optional_default(ghost_update, .true.)

  do idir = 1, der%dim
    call X(derivatives_perform) (der%grad(idir), der, ff, op_ff(:, idir), ghost_update_, set_bc_)

    set_bc_       = .false. ! there is no need to update again
    ghost_update_ = .false. ! the boundary or ghost points
  end do

  ! Convert the gradient, which is a covariant vector, to Cartesian coordinates
  if(optional_default(to_cartesian, .true.)) then
    do ip = 1, der%mesh%np
      call der%mesh%coord_system%covector_to_cartesian(der%mesh%x(ip, :), op_ff(ip, :))
    end do
  end if

  call profiling_out(TOSTRING(X(GRADIENT)))
  POP_SUB(X(derivatives_grad))
end subroutine X(derivatives_grad)

! ---------------------------------------------------------
!> @brief apply the partial derivative along dir to a mesh function
!!
!! @note the initial mesh function needs to be defined including ghost and boundary points!
!!! As the ghost points are updated, the initial mesh function needs to be declared intent(inout).

subroutine X(derivatives_partial)(der, ff, op_ff, dir, ghost_update, set_bc)
  type(derivatives_t),       intent(in)    :: der          !< derivatives object
  R_TYPE, contiguous,        intent(inout) :: ff(:)        !< initial mesh function (1:der%mesh%np_part)
  R_TYPE, contiguous,        intent(out)   :: op_ff(:)     !< final mesh function (1:der%mesh%np)
  integer,                   intent(in)    :: dir          !< direction along which to calculate the partial derivative
  logical, optional,         intent(in)    :: ghost_update !< flag whether to perform the ghost update (default .true.)
  logical, optional,         intent(in)    :: set_bc       !< flag whether to set the boundary conditions (default .true.)

  logical :: set_bc_, ghost_update_

  PUSH_SUB(X(derivatives_partial))

  set_bc_ = optional_default(set_bc, .true.)
  ghost_update_ = optional_default(ghost_update, .true.)

  call X(derivatives_perform) (der%grad(dir), der, ff, op_ff, ghost_update_, set_bc_)

  POP_SUB(X(derivatives_partial))
end subroutine X(derivatives_partial)


! ---------------------------------------------------------
!> @brief apply the divergence operator to a vector of mesh functions
!!
!! @note the initial mesh function needs to be defined including ghost and boundary points!
!! As the ghost points are updated, the initial mesh function needs to be declared intent(inout).
!
subroutine X(derivatives_div)(der, ff, op_ff, ghost_update, set_bc, to_cartesian)
  type(derivatives_t),          intent(in)    :: der          !< derivatives object
  R_TYPE,  contiguous, target,  intent(inout) :: ff(:,:)      !< initial mesh function vector ff(1:der%mesh%np_part, 1:der%dim)
  R_TYPE,  contiguous,          intent(out)   :: op_ff(:)     !< final mesh function op_ff(1:der%mesh%np)
  logical, optional,            intent(in)    :: ghost_update !< flag whether to perform the ghost update (default .true.)
  logical, optional,            intent(in)    :: set_bc       !< flag whether to apply the boundary conditions (default .true.)
  logical, optional,            intent(in)    :: to_cartesian !< flag whether conversion to cartesian coords in needed

  R_TYPE, allocatable         :: tmp(:)
  R_TYPE, contiguous, pointer :: ff_uvw(:,:)
  integer                     :: idir, ii, ip

  PUSH_SUB(X(derivatives_div))
  call profiling_in(TOSTRING(X(DIVERGENCE)))

  ASSERT(ubound(ff, DIM=2) >= der%dim)

  ! div_xyw (F)= div_uvw (BF), where B
  select type (coord_system => der%mesh%coord_system)
  type is (cartesian_t)
    ff_uvw => ff
  class default
    if(optional_default(to_cartesian, .true.)) then
      SAFE_ALLOCATE(ff_uvw(1:der%mesh%np_part, 1:der%dim))
      !$omp parallel do
      do ip = 1, der%mesh%np_part
        call der%mesh%coord_system%vector_from_cartesian(der%mesh%x(ip, :), ff_uvw(ip, :), src=ff(ip, :))
      end do
    else
      ff_uvw => ff
    end if
  end select

  call X(derivatives_perform) (der%grad(1), der, ff_uvw(:, 1), op_ff, ghost_update, set_bc)

  SAFE_ALLOCATE(tmp(1:der%mesh%np))

  do idir = 2, der%dim
    call X(derivatives_perform) (der%grad(idir), der, ff_uvw(:, idir), tmp, ghost_update, set_bc)

    !$omp parallel do
    do ii = 1, der%mesh%np
      op_ff(ii) = op_ff(ii) + tmp(ii)
    end do
  end do

  SAFE_DEALLOCATE_A(tmp)
  select type (coord_system => der%mesh%coord_system)
  type is (cartesian_t)
    nullify(ff_uvw)
  class default
    if(optional_default(to_cartesian, .true.)) then
      SAFE_DEALLOCATE_P(ff_uvw)
    else
      nullify(ff_uvw)
    end if
  end select

  call profiling_out(TOSTRING(X(DIVERGENCE)))
  POP_SUB(X(derivatives_div))
end subroutine X(derivatives_div)



! ---------------------------------------------------------
!> @brief apply the curl operator to a vector of mesh functions
!!
!! @note the initial mesh function needs to be defined including ghost and boundary points!
!! As the ghost points are updated, the initial mesh function needs to be declared intent(inout).
!
subroutine X(derivatives_curl)(der, ff, op_ff, ghost_update, set_bc)
  type(derivatives_t), intent(in)    :: der          !< derivatives object
  R_TYPE, contiguous,  intent(inout) :: ff(:,:)      !< initial mesh function vector ff(1:der%mesh%np_part, 1:der%dim)
  R_TYPE, contiguous,  intent(out)   :: op_ff(:,:)   !< final mesh function vector op_ff(1:der%mesh%np, 1:der%dim)
  logical, optional,   intent(in)    :: ghost_update !< flag whether to perform the ghost update (default .true.)
  logical, optional,   intent(in)    :: set_bc       !< flag whether to apply the boundary conditions (default .true.)

  integer, parameter    :: curl_dim(3) = (/-1, 1, 3/)
  R_TYPE, allocatable   :: tmp(:)
  integer               :: ii, np

  PUSH_SUB(X(derivatives_curl))
  call profiling_in(TOSTRING(X(CURL)))

  ASSERT(der%dim == 2 .or. der%dim == 3)
  ASSERT(ubound(ff,    DIM=2) >= der%dim)
  ASSERT(ubound(op_ff, DIM=2) >= curl_dim(der%dim))

  select type (coord_system => der%mesh%coord_system)
  type is (affine_coordinates_t)
    ! Note that we used 'type is', not 'class is', so that the Cartesian case is
    ! treated by the default clause.
    ASSERT(.false.)
  class default
    ! All is good
  end select

  SAFE_ALLOCATE(tmp(1:der%mesh%np_part))

  op_ff(:,:) = R_TOTYPE(M_ZERO)
  np = der%mesh%np

  select case (der%dim)
  case (3)
    call X(derivatives_perform) (der%grad(3), der, ff(:,1), tmp, ghost_update, set_bc)
    do ii = 1, np
      op_ff(ii, 2) = op_ff(ii, 2) + tmp(ii)
    end do
    call X(derivatives_perform) (der%grad(2), der, ff(:,1), tmp, .false., .false.)
    do ii = 1, np
      op_ff(ii, 3) = op_ff(ii, 3) - tmp(ii)
    end do

    call X(derivatives_perform) (der%grad(3), der, ff(:,2), tmp, ghost_update, set_bc)
    do ii = 1, np
      op_ff(ii, 1) = op_ff(ii, 1) - tmp(ii)
    end do
    call X(derivatives_perform) (der%grad(1), der, ff(:,2), tmp, .false., .false.)
    do ii = 1, np
      op_ff(ii, 3) = op_ff(ii, 3) + tmp(ii)
    end do

    call X(derivatives_perform) (der%grad(2), der, ff(:,3), tmp, ghost_update, set_bc)
    do ii = 1, np
      op_ff(ii, 1) = op_ff(ii, 1) + tmp(ii)
    end do
    call X(derivatives_perform) (der%grad(1), der, ff(:,3), tmp, .false., .false.)
    do ii = 1, np
      op_ff(ii, 2) = op_ff(ii, 2) - tmp(ii)
    end do

  case (2)
    call X(derivatives_perform) (der%grad(2), der, ff(:,1), tmp, ghost_update, set_bc)
    do ii = 1, np
      op_ff(ii, 1) = op_ff(ii, 1) - tmp(ii)
    end do
    call X(derivatives_perform) (der%grad(1), der, ff(:,2), tmp, .false., .false.)
    do ii = 1, np
      op_ff(ii, 1) = op_ff(ii, 1) + tmp(ii)
    end do
  end select

  SAFE_DEALLOCATE_A(tmp)
  call profiling_out(TOSTRING(X(CURL)))
  POP_SUB(X(derivatives_curl))
end subroutine X(derivatives_curl)


! ----------------------------------------------------------
!> @brief unit test for derivatives
!!
!! This will be called from the test run mode.
subroutine X(derivatives_test)(this, namespace, repetitions, min_blocksize, max_blocksize)
  type(derivatives_t), intent(in) :: this
  type(namespace_t),   intent(in) :: namespace
  integer,             intent(in) :: repetitions
  integer,             intent(in) :: min_blocksize
  integer,             intent(in) :: max_blocksize

  R_TYPE, allocatable :: ff(:), opff(:, :), gradff(:, :), curlff(:, :), res(:), resgrad(:, :), rescurl(:, :)
  R_TYPE :: aa, bb, cc
  integer :: ip, idir, ist, ib
  type(batch_t) :: ffb, opffb
  type(batch_t) :: gradffb(this%mesh%box%dim)
  integer :: blocksize, itime
  logical :: packstates
  real(real64)   :: stime, etime
  character(len=20) :: type
  real(real64) :: norm

  call parse_variable(namespace, 'StatesPack', .true., packstates)

  SAFE_ALLOCATE(ff(1:this%mesh%np_part))
  SAFE_ALLOCATE(opff(1:this%mesh%np, 1:this%mesh%box%dim))
  SAFE_ALLOCATE(gradff(1:this%mesh%np, 1:this%mesh%box%dim))
  SAFE_ALLOCATE(res(1:this%mesh%np_part))
  SAFE_ALLOCATE(resgrad(1:this%mesh%np, 1:this%mesh%box%dim))
  SAFE_ALLOCATE(curlff(1:this%mesh%np, 1:this%mesh%box%dim))

#ifdef R_TREAL
  type = 'real'
#else
  type = 'complex'
#endif

  ! Note: here we need to use a constant function or anything that
  ! is constant at the borders, since we assume that all boundary
  ! points have equal values to optimize the application of the nl-operator.

  aa = R_TOTYPE(1.0)/this%mesh%box%bounding_box_l(1)
  bb = R_TOTYPE(10.0)
  cc = R_TOTYPE(100.0)

#ifdef R_TCOMPLEX
  ! we make things more "complex"
  aa = aa + M_ZI*R_TOTYPE(0.01)
  bb = bb*exp(M_ZI*R_TOTYPE(0.345))
  cc = cc - M_ZI*R_TOTYPE(50.0)
#endif


  do ip = 1, this%mesh%np_part
    ff(ip) = bb*exp(-aa*sum(this%mesh%x(ip, :)**2)) + cc
  end do
  do ip = 1, this%mesh%np
    do idir = 1, this%mesh%box%dim
      gradff(ip, idir) = -M_TWO*aa*bb*this%mesh%x(ip, idir)*exp(-aa*sum(this%mesh%x(ip, :)**2))
    end do
  end do

  ! curl only needed in 3D
  if (this%mesh%box%dim == 3) then
    do ip = 1, this%mesh%np
      curlff(ip, 1) = -M_TWO*aa*bb*exp(-aa*sum(this%mesh%x(ip, :)**2))*(this%mesh%x(ip, 2) - this%mesh%x(ip, 3))
      curlff(ip, 2) = -M_TWO*aa*bb*exp(-aa*sum(this%mesh%x(ip, :)**2))*(this%mesh%x(ip, 3) - this%mesh%x(ip, 1))
      curlff(ip, 3) = -M_TWO*aa*bb*exp(-aa*sum(this%mesh%x(ip, :)**2))*(this%mesh%x(ip, 1) - this%mesh%x(ip, 2))
    end do
  end if

  message(1) = "Testing Laplacian"
  call messages_info(1, namespace=namespace)

  ! test Laplacian
  blocksize = min_blocksize
  do

    call X(batch_init)(ffb, 1, 1, blocksize, this%mesh%np_part)
    call X(batch_init)(opffb, 1, 1, blocksize, this%mesh%np)

    do ist = 1, blocksize
      call batch_set_state(ffb, ist, this%mesh%np_part, ff)
    end do

    if (packstates) then
      call ffb%do_pack()
      call opffb%do_pack(copy = .false.)
    end if

    if (repetitions > 1) then
      call X(derivatives_batch_perform)(this%lapl, this, ffb, opffb, set_bc = .false., factor = M_HALF)
    end if

    stime = loct_clock()
    do itime = 1, repetitions
      call X(derivatives_batch_perform)(this%lapl, this, ffb, opffb, set_bc = .false., factor = M_HALF)
    end do
    etime = (loct_clock() - stime)/real(repetitions, real64)

    call batch_get_state(opffb, blocksize, this%mesh%np, res)

    call ffb%end()
    call opffb%end()

    do ip = 1, this%mesh%np
      res(ip) = M_TWO*res(ip) - &
        (M_FOUR*aa**2*bb*sum(this%mesh%x(ip, :)**2)*exp(-aa*sum(this%mesh%x(ip, :)**2)) &
        - this%mesh%box%dim*M_TWO*aa*bb*exp(-aa*sum(this%mesh%x(ip, :)**2)))
    end do

    write(message(1), '(3a,i3,a,es17.10,a,f8.3)') &
      'Laplacian ', trim(type),  &
      ' bsize = ', blocksize,    &
      ' , error = ', X(mf_nrm2)(this%mesh, res), &
      ' , Gflops = ',  &
#ifdef R_TREAL
      blocksize*this%mesh%np*M_TWO*this%lapl%stencil%size/(etime*1.0e9_real64)
#else
    blocksize*this%mesh%np*M_FOUR*this%lapl%stencil%size/(etime*1.0e9_real64)
#endif

    call messages_info(1, namespace=namespace)

    blocksize = 2*blocksize
    if (blocksize > max_blocksize) exit

  end do

  message(1) = "Testing Gradient"
  call messages_info(1, namespace=namespace)

  ! test Gradient
  blocksize = min_blocksize
  do
    call X(batch_init)(ffb, 1, 1, blocksize, this%mesh%np_part)

    do ist = 1, blocksize
      call batch_set_state(ffb, ist, this%mesh%np_part, ff)
    end do

    if (packstates) then
      call ffb%do_pack()
    end if

    if (repetitions > 1) then
      call X(derivatives_batch_grad)(this, ffb, gradffb, set_bc=.false.)
    end if

    stime = loct_clock()
    do itime = 1, repetitions
      call X(derivatives_batch_grad)(this, ffb, gradffb, set_bc=.false.)
    end do
    etime = (loct_clock() - stime)/real(repetitions, real64)

    do idir = 1, this%mesh%box%dim
      call batch_get_state(gradffb(idir), blocksize, this%mesh%np, resgrad(:, idir))
    end do

    call ffb%end()
    do idir = 1, this%mesh%box%dim
      call gradffb(idir)%end()
    end do

    do idir = 1, this%mesh%box%dim
      do ip = 1, this%mesh%np
        resgrad(ip, idir) = resgrad(ip, idir) - gradff(ip, idir)
      end do
    end do

    write(message(1), '(3a,i3,a,es17.10,a,f8.3)') &
      'Batch gradient ', trim(type),  &
      ' bsize = ', blocksize,    &
      ' , error (x direction) = ', X(mf_nrm2)(this%mesh, resgrad(:, 1)), &
      ' , Gflops = ',  &
#ifdef R_TREAL
      blocksize*this%mesh%np*M_TWO*this%grad(1)%stencil%size*this%mesh%box%dim/(etime*1.0e9_real64)
#else
    blocksize*this%mesh%np*M_FOUR*this%grad(1)%stencil%size*this%mesh%box%dim/(etime*1.0e9_real64)
#endif
    call messages_info(1, namespace=namespace)

    blocksize = 2*blocksize
    if (blocksize > max_blocksize) exit
  end do

  call X(derivatives_grad)(this, ff, opff, set_bc = .false.)

  do idir = 1, this%mesh%box%dim
    do ip = 1, this%mesh%np
      opff(ip, idir) = opff(ip, idir) - (-M_TWO*aa*bb*this%mesh%x(ip, idir)*exp(-aa*sum(this%mesh%x(ip, :)**2)))
    end do
  end do

  message(1) = ''
  call messages_info(1, namespace=namespace)

  write(message(1), '(3a, es17.10)') 'Gradient ', trim(type),  &
    ' err = ', X(mf_nrm2)(this%mesh, this%mesh%box%dim, opff)
  call messages_info(1, namespace=namespace)

  message(1) = ''
  call messages_info(1, namespace=namespace)

  ! test curl for 3D case
  if (this%mesh%box%dim == 3) then
    message(1) = "Testing curl"
    call messages_info(1, namespace=namespace)

    do blocksize = 3, 9, 3
      call X(batch_init)(ffb, 1, 1, blocksize, this%mesh%np_part)

      do ist = 1, blocksize
        call batch_set_state(ffb, ist, this%mesh%np_part, ff)
      end do

      if (packstates) then
        call ffb%do_pack()
      end if

      ! test for one blocksize without precomputed gradient
      if (blocksize == 9) then
        call X(derivatives_batch_curl)(this, ffb, set_bc=.false.)
      else
        call X(derivatives_batch_grad)(this, ffb, gradffb, set_bc=.false.)
        call X(derivatives_batch_curl_from_gradient)(this, ffb, gradffb)
      end if

      SAFE_ALLOCATE(rescurl(1:this%mesh%np, 1:blocksize))
      do ist = 1, blocksize
        call batch_get_state(ffb, ist, this%mesh%np, rescurl(:, ist))
      end do

      call ffb%end()
      do idir = 1, this%mesh%box%dim
        call gradffb(idir)%end()
      end do

      do ip = 1, this%mesh%np
        do ib = 0, blocksize-1, this%mesh%box%dim
          do idir = 1, this%mesh%box%dim
            rescurl(ip, ib+idir) = rescurl(ip, ib+idir) - curlff(ip, idir)
          end do
        end do
      end do

      norm = M_ZERO
      do ist = 1, blocksize
        norm = norm + X(mf_nrm2)(this%mesh, rescurl(:, ist))
      end do
      SAFE_DEALLOCATE_A(rescurl)

      write(message(1), '(3a,i3,a,es17.10,a,f8.3)') &
        'Batch curl ', trim(type), ' bsize = ', blocksize, ' , error = ', norm
      call messages_info(1, namespace=namespace)
    end do
  end if

  message(1) = ''
  call messages_info(1, namespace=namespace)

  SAFE_DEALLOCATE_A(ff)
  SAFE_DEALLOCATE_A(opff)
  SAFE_DEALLOCATE_A(gradff)
  SAFE_DEALLOCATE_A(res)
  SAFE_DEALLOCATE_A(resgrad)
  SAFE_DEALLOCATE_A(curlff)

end subroutine X(derivatives_test)

!=====================================================================================
! Batched versions of the routines:

!> @brief apply a non-local operator to a batch (1st part)
!!
!! This 1st part stores important data in the handle,
!! sets the boundaries (if requested) and starts the
!! communication for the ghost update (if requested)
!
subroutine X(derivatives_batch_start)(op, der, ff, opff, handle, ghost_update, set_bc, factor)
  type(nl_operator_t),       target, intent(in)    :: op             !< the operator to be applied
  type(derivatives_t),       target, intent(in)    :: der            !< the derivatives object
  class(batch_t),            target, intent(inout) :: ff             !< initial batch
  class(batch_t),            target, intent(inout) :: opff           !< finial batch (after application of the operator)
  type(derivatives_handle_batch_t),  intent(out)   :: handle         !< handle for start/finish calls
  logical,                 optional, intent(in)    :: ghost_update   !< whether to perform ghost updates (default=.true.)
  logical,                 optional, intent(in)    :: set_bc         !< whether to set the boundary conditions (default=.true.)
  real(real64),            optional, intent(in)    :: factor         !< optional scaling factor

  PUSH_SUB(X(derivatives_batch_start))

  handle%ghost_update = optional_default(ghost_update, .true.)

  handle%op   => op
  handle%der  => der
  handle%ff   => ff
  handle%opff => opff

  if (present(factor)) then
    handle%factor_present = .true.
    handle%factor = factor
  else
    handle%factor_present = .false.
  end if

  ASSERT(handle%ff%nst_linear == handle%opff%nst_linear)

  if (optional_default(set_bc, .true.)) then
    call boundaries_set(der%boundaries, der%mesh, ff)
    ASSERT(.not. der%boundaries%spiral)
  end if

#ifdef HAVE_MPI

  if (derivatives_overlap(der) .and. der%mesh%parallel_in_domains .and. handle%ghost_update) then
    call X(ghost_update_batch_start)(der%mesh%pv, ff, handle%pv_h)
  end if
#endif

  POP_SUB(X(derivatives_batch_start))
end subroutine X(derivatives_batch_start)


! ---------------------------------------------------------
!> @brief apply a non-local operator to a batch (2nd part)
!!
!! This 2nd part finished the communication and applies
!! the operator. Here, the operator is applied to the inner points
!! before the communication is finished, to overlap calculation
!! and communication, if possible. After the communication is complete,
!! the operator is applied to the outer points.
!
subroutine X(derivatives_batch_finish)(handle, async)
  type(derivatives_handle_batch_t), intent(inout) :: handle
  logical,                optional, intent(in)    :: async

  logical :: done

  PUSH_SUB(X(derivatives_batch_finish))

  done = .false.

#ifdef HAVE_MPI
  if (derivatives_overlap(handle%der) .and. handle%der%mesh%parallel_in_domains .and. handle%ghost_update) then

    done = .true.

    if (handle%factor_present) then
      call X(nl_operator_operate_batch)(handle%op, handle%ff, handle%opff, &
        ghost_update = .false., points = OP_INNER, factor = handle%factor, async=async)
    else
      call X(nl_operator_operate_batch)(handle%op, handle%ff, handle%opff, ghost_update=.false., points=OP_INNER, async=async)
    end if

    call X(ghost_update_batch_finish)(handle%pv_h)

    if (handle%factor_present) then
      call X(nl_operator_operate_batch)(handle%op, handle%ff, handle%opff, &
        ghost_update = .false., points = OP_OUTER, factor = handle%factor, async=async)
    else
      call X(nl_operator_operate_batch)(handle%op, handle%ff, handle%opff, ghost_update = .false., points = OP_OUTER, async=async)
    end if

  end if
#endif

  if (.not. done) then
    if (handle%factor_present) then
      call X(nl_operator_operate_batch)(handle%op, handle%ff, handle%opff, ghost_update = .false., factor = handle%factor)
    else
      call X(nl_operator_operate_batch)(handle%op, handle%ff, handle%opff, ghost_update = .false.)
    end if
  end if

  POP_SUB(X(derivatives_batch_finish))
end subroutine X(derivatives_batch_finish)


! ---------------------------------------------------------
!> @brief apply an operator to a bach of mesh functions
!!
!! This is a wrapper of the corresponding start and finish routines.
!
subroutine X(derivatives_batch_perform)(op, der, ff, opff, ghost_update, set_bc, factor, async)
  type(nl_operator_t), intent(in)    :: op             !< the operator to be applied
  type(derivatives_t), intent(in)    :: der            !< the derivatives object
  class(batch_t),      intent(inout) :: ff             !< initial batch
  class(batch_t),      intent(inout) :: opff           !< finial batch (after application of the operator)
  logical,   optional, intent(in)    :: ghost_update   !< whether to perform ghost updates (default=.true.)
  logical,   optional, intent(in)    :: set_bc         !< whether to set the boundary conditions (default=.true.)
  real(real64),     optional, intent(in)    :: factor         !< optional scaling factor
  logical,   optional, intent(in)    :: async

  type(derivatives_handle_batch_t) :: handle

  PUSH_SUB(X(derivatives_batch_perform))

  call ff%check_compatibility_with(opff)

  call X(derivatives_batch_start)(op, der, ff, opff, handle, ghost_update, set_bc, factor)
  call X(derivatives_batch_finish)(handle)

  POP_SUB(X(derivatives_batch_perform))

end subroutine X(derivatives_batch_perform)



!> @brief apply the gradient to a batch of mesh functions
!!
subroutine X(derivatives_batch_grad)(der, ffb, opffb, ghost_update, set_bc, to_cartesian, metric, factor)
  type(derivatives_t),         intent(in)    :: der           !< derivatives object
  class(batch_t),              intent(inout) :: ffb           !< initial batch
  class(batch_t),              intent(inout) :: opffb(:)      !< final vector of batches
  logical, optional,           intent(in)    :: ghost_update  !< optional flag whether to perform the ghost update (default .true.)
  logical, optional,           intent(in)    :: set_bc        !< optional flag whether to set the boundary conditions (default .true.)
  logical, optional,           intent(in)    :: to_cartesian  !< optional flag whether to convert to cartesian coods (currently unused!)
  real(real64),   optional,           intent(in)    :: metric(:,:)   !< optional metric for affine coordinate systems
  real(real64),   optional,           intent(in)    :: factor        !< optional scaling factor

  integer :: idir
  logical :: set_bc_, ghost_update_

  PUSH_SUB(X(derivatives_batch_grad))
  call profiling_in(TOSTRING(X(BATCH_GRADIENT)))

  ASSERT(size(opffb) == der%dim)

  set_bc_ = optional_default(set_bc, .true.)
  ghost_update_ = optional_default(ghost_update, .true.)

  do idir = 1, der%dim
    call ffb%copy_to(opffb(idir))
    call X(derivatives_batch_perform)(der%grad(idir), der, ffb, opffb(idir), &
      ghost_update=ghost_update_, set_bc=set_bc_, factor=factor, async=.true.)
    set_bc_       = .false. ! there is no need to update again
    ghost_update_ = .false. ! the boundary or ghost points
  end do

  select type (coord_system => der%mesh%coord_system)
  type is (affine_coordinates_t)
    call X(batch_vector_uvw_to_xyz)(der, coord_system, opffb, metric=metric)
  class default
    call accel_finish()
  end select

  call profiling_out(TOSTRING(X(BATCH_GRADIENT)))
  POP_SUB(X(derivatives_batch_grad))
end subroutine X(derivatives_batch_grad)

!> @brief transform a vector of batches from affine to cartesian coordinates
!
subroutine X(batch_vector_uvw_to_xyz)(der, coord_system, uvw, xyz, metric)
  type(derivatives_t),                intent(in)    :: der
  type(affine_coordinates_t), target, intent(in)    :: coord_system
  class(batch_t), target,             intent(inout) :: uvw(:)
  class(batch_t), target, optional,   intent(inout) :: xyz(:)
  real(real64),   target, optional,          intent(in)    :: metric(:,:) !< Transpose of the matrix

  class(batch_t), pointer:: xyz_(:)
  class(batch_t), pointer:: uvw_(:)
  integer :: ist, ip, idim1, idim2
  R_TYPE, allocatable :: tmp(:)
  real(real64), pointer :: metric_(:,:)

  PUSH_SUB(X(batch_vector_uvw_to_xyz))
  if (present(xyz)) then
    xyz_ => xyz
  else
    xyz_ => uvw
  end if
  ! this pointer assigment is needed as a workaround for gcc 12
  ! otherwise it throws errors related to openmp and polymorphic arrays
  uvw_ => uvw

  if(present(metric)) then
    metric_ => metric
  else
    ! We want the transpose of this one. The transpose is done below.
    metric_ => coord_system%basis%change_of_basis_matrix
  end if

  ASSERT(size(uvw) == der%dim)
  ASSERT(size(xyz_) == der%dim)
  do idim1 = 1, der%dim
    ASSERT(uvw(idim1)%status() == xyz_(idim1)%status())
    ASSERT(uvw(idim1)%status() == uvw(1)%status())
  end do

  SAFE_ALLOCATE(tmp(1:der%dim))

  select case (uvw(1)%status())
  case (BATCH_NOT_PACKED)
    do ist = 1, uvw_(1)%nst_linear
      !$omp parallel do private(tmp, ip, idim1, idim2)
      do ip = 1, der%mesh%np
        tmp = R_TOTYPE(M_ZERO)
        ! Grad_xyw = Bt Grad_uvw, see Chelikowsky after Eq. 10
        do idim2 = 1, der%dim
          do idim1 = 1, der%dim
            tmp(idim1) = tmp(idim1) + metric_(idim2, idim1) * uvw_(idim2)%X(ff_linear)(ip, ist)
          end do
        end do
        do idim1 = 1, der%dim
          xyz_(idim1)%X(ff_linear)(ip, ist) = tmp(idim1)
        end do
      end do
    end do
  case (BATCH_PACKED)
    !$omp parallel do private(tmp, ip, ist, idim1, idim2)
    do ip = 1, der%mesh%np
      do ist = 1, uvw_(1)%nst_linear
        tmp = R_TOTYPE(M_ZERO)
        ! Grad_xyw = Bt Grad_uvw, see Chelikowsky after Eq. 10
        do idim2 = 1, der%dim
          do idim1 = 1, der%dim
            tmp(idim1) = tmp(idim1) + metric_(idim2, idim1) * uvw_(idim2)%X(ff_pack)(ist, ip)
          end do
        end do
        do idim1 = 1, der%dim
          xyz_(idim1)%X(ff_pack)(ist, ip) = tmp(idim1)
        end do
      end do
    end do
  case (BATCH_DEVICE_PACKED)
    call uvw_to_xyz_opencl()
  end select

  SAFE_DEALLOCATE_A(tmp)

  POP_SUB(X(batch_vector_uvw_to_xyz))

contains
  subroutine uvw_to_xyz_opencl
    integer(int64) :: localsize, dim3, dim2
    type(accel_mem_t) :: matrix_buffer

    PUSH_SUB(uvw_to_xyz_opencl)

    call accel_create_buffer(matrix_buffer, ACCEL_MEM_READ_ONLY, TYPE_FLOAT, der%dim**2)
    call accel_write_buffer(matrix_buffer, der%dim**2, transpose(metric_(1:der%dim,1:der%dim)), async=.true.)

    call accel_set_kernel_arg(kernel_uvw_xyz, 0, der%mesh%np)
    call accel_set_kernel_arg(kernel_uvw_xyz, 1, matrix_buffer)
    call accel_set_kernel_arg(kernel_uvw_xyz, 2, uvw(1)%ff_device)
    call accel_set_kernel_arg(kernel_uvw_xyz, 3, log2(int(uvw(1)%pack_size_real(1), int32)))
    call accel_set_kernel_arg(kernel_uvw_xyz, 4, xyz_(1)%ff_device)
    call accel_set_kernel_arg(kernel_uvw_xyz, 5, log2(int(xyz_(1)%pack_size_real(1), int32)))
    if (der%dim > 1) then
      call accel_set_kernel_arg(kernel_uvw_xyz, 6, uvw(2)%ff_device)
      call accel_set_kernel_arg(kernel_uvw_xyz, 7, log2(int(uvw(2)%pack_size_real(1), int32)))
      call accel_set_kernel_arg(kernel_uvw_xyz, 8, xyz_(2)%ff_device)
      call accel_set_kernel_arg(kernel_uvw_xyz, 9, log2(int(xyz_(2)%pack_size_real(1), int32)))
    end if
    if (der%dim > 2) then
      call accel_set_kernel_arg(kernel_uvw_xyz, 10, uvw(3)%ff_device)
      call accel_set_kernel_arg(kernel_uvw_xyz, 11, log2(int(uvw(3)%pack_size_real(1), int32)))
      call accel_set_kernel_arg(kernel_uvw_xyz, 12, xyz_(3)%ff_device)
      call accel_set_kernel_arg(kernel_uvw_xyz, 13, log2(int(xyz_(3)%pack_size_real(1), int32)))
    end if

    localsize = accel_kernel_workgroup_size(kernel_uvw_xyz)/uvw(1)%pack_size_real(1)

    dim3 = der%mesh%np/(accel_max_size_per_dim(2)*localsize) + 1
    dim2 = min(accel_max_size_per_dim(2)*localsize, pad(der%mesh%np, localsize))

    call accel_kernel_run(kernel_uvw_xyz, (/uvw(1)%pack_size_real(1), dim2, dim3/), &
      (/uvw(1)%pack_size_real(1), localsize, 1_int64/))
    call accel_finish()

    call accel_release_buffer(matrix_buffer)

    POP_SUB(uvw_to_xyz_opencl)
  end subroutine uvw_to_xyz_opencl
end subroutine X(batch_vector_uvw_to_xyz)

! ---------------------------------------------------------
!> @brief apply the curl to a batch of mesh functions
!!
!! This routine computes first a gradient. If the gradient is known,
!! consider using derivatives_batch_curl_from_gradient instead.
subroutine X(derivatives_batch_curl)(der, ffb, ghost_update, set_bc)
  type(derivatives_t),           intent(in)    :: der          !< the derivatives object
  class(batch_t),                intent(inout) :: ffb          !< the initial batch; ffb will be overwritten by its curl
  logical,                optional, intent(in) :: ghost_update !< optional flag whether to perform the ghost update (default .true.)
  logical,                optional, intent(in) :: set_bc       !< optional flag whether to set the boundary conditions (default .true.)

  class(batch_t), allocatable :: gradb(:)
  integer :: idir

  PUSH_SUB(X(derivatives_batch_curl))
  call profiling_in(TOSTRING(X(CURL_BATCH)))

  allocate(gradb(1:der%dim), mold=ffb)
  call X(derivatives_batch_grad)(der, ffb, gradb, ghost_update=ghost_update, set_bc=set_bc)

  call X(derivatives_batch_curl_from_gradient)(der, ffb, gradb)

  do idir = 1, der%dim
    call gradb(idir)%end()
  end do
  SAFE_DEALLOCATE_A(gradb)

  call profiling_out(TOSTRING(X(CURL_BATCH)))
  POP_SUB(X(derivatives_batch_curl))
end subroutine X(derivatives_batch_curl)

! ---------------------------------------------------------
!> @brief calculate the curl from a batch and its gradient
!!
!! If the gradient of the batch already has been calculated, this routine is reuses it to get the curl
subroutine X(derivatives_batch_curl_from_gradient)(der, ffb, gradb)
  type(derivatives_t), intent(in)    :: der      !< the derivatives object
  class(batch_t),      intent(inout) :: ffb      !< on output: batch of the curl
  class(batch_t),      intent(in)    :: gradb(:) !< batch of the gradients of the mesh functions

  integer :: ip, ist, ivec
  integer(int64) :: localsize, dim2, dim3

  PUSH_SUB(X(derivatives_batch_curl_from_gradient))

  ASSERT(der%dim == 3)
  ASSERT(size(gradb) == der%dim)
  ASSERT(modulo(ffb%nst_linear, der%dim) == 0)
  do ist = 1, 3
    ASSERT(ffb%status() == gradb(ist)%status())
  end do

  select case (ffb%status())
  case (BATCH_NOT_PACKED)
    ! loop over ivec in case we have several vectors per batch
    do ivec = 0, ffb%nst_linear-1, der%dim
      !$omp parallel do
      do ip = 1, der%mesh%np
        ffb%X(ff_linear)(ip, ivec+1) = gradb(2)%X(ff_linear)(ip, ivec+3) - gradb(3)%X(ff_linear)(ip, ivec+2)
        ffb%X(ff_linear)(ip, ivec+2) = gradb(3)%X(ff_linear)(ip, ivec+1) - gradb(1)%X(ff_linear)(ip, ivec+3)
        ffb%X(ff_linear)(ip, ivec+3) = gradb(1)%X(ff_linear)(ip, ivec+2) - gradb(2)%X(ff_linear)(ip, ivec+1)
      end do
    end do
  case (BATCH_PACKED)
    !$omp parallel do private(ivec)
    do ip = 1, der%mesh%np
      ! loop over ivec in case we have several vectors per batch
      do ivec = 0, ffb%nst_linear-1, der%dim
        ffb%X(ff_pack)(ivec+1, ip) = gradb(2)%X(ff_pack)(ivec+3, ip) - gradb(3)%X(ff_pack)(ivec+2, ip)
        ffb%X(ff_pack)(ivec+2, ip) = gradb(3)%X(ff_pack)(ivec+1, ip) - gradb(1)%X(ff_pack)(ivec+3, ip)
        ffb%X(ff_pack)(ivec+3, ip) = gradb(1)%X(ff_pack)(ivec+2, ip) - gradb(2)%X(ff_pack)(ivec+1, ip)
      end do
    end do
  case (BATCH_DEVICE_PACKED)
    call accel_set_kernel_arg(aX(kernel_,curl), 0, der%mesh%np)
    call accel_set_kernel_arg(aX(kernel_,curl), 1, ffb%ff_device)
    call accel_set_kernel_arg(aX(kernel_,curl), 2, log2(int(ffb%pack_size(1), int32)))
    call accel_set_kernel_arg(aX(kernel_,curl), 3, gradb(1)%ff_device)
    call accel_set_kernel_arg(aX(kernel_,curl), 4, log2(int(gradb(1)%pack_size(1), int32)))
    call accel_set_kernel_arg(aX(kernel_,curl), 5, gradb(2)%ff_device)
    call accel_set_kernel_arg(aX(kernel_,curl), 6, log2(int(gradb(2)%pack_size(1), int32)))
    call accel_set_kernel_arg(aX(kernel_,curl), 7, gradb(3)%ff_device)
    call accel_set_kernel_arg(aX(kernel_,curl), 8, log2(int(gradb(3)%pack_size(1), int32)))

    localsize = accel_kernel_workgroup_size(aX(kernel_,curl))/ffb%pack_size(1)

    dim3 = der%mesh%np/(accel_max_size_per_dim(2)*localsize) + 1
    dim2 = min(accel_max_size_per_dim(2)*localsize, pad(der%mesh%np, localsize))

    call accel_kernel_run(aX(kernel_,curl), (/ffb%pack_size(1), dim2, dim3/), &
      (/ffb%pack_size(1), localsize, 1_int64/))
    call accel_finish()
  end select

  POP_SUB(X(derivatives_batch_curl_from_gradient))
end subroutine X(derivatives_batch_curl_from_gradient)

! ---------------------------------------------------------
!> @brief calculate the divergence of a vector of batches
!!
subroutine X(derivatives_batch_div)(der, ffb, opffb, ghost_update, set_bc, to_cartesian)
  type(derivatives_t), intent(in)    :: der           !< the derivatives object
  class(batch_t),      intent(inout) :: ffb(:)        !< vector of initial batches
  class(batch_t),      intent(inout) :: opffb         !< batch containing the divergence
  logical, optional,   intent(in)    :: ghost_update  !< optional flag whether to perform the ghost update (default .true.)
  logical, optional,   intent(in)    :: set_bc        !< optional flag whether to set the boundary conditions (default .true.)
  logical, optional,   intent(in)    :: to_cartesian  !< optional flag whether to convert to cartesian coords (default .true.)

  class(batch_t), allocatable :: tmpb
  integer :: idir
  logical :: set_bc_, ghost_update_, to_cartesian_

  PUSH_SUB(X(derivatives_batch_div))
  call profiling_in(TOSTRING(X(BATCH_DIVERGENCE)))

  set_bc_ = optional_default(set_bc, .true.)
  ghost_update_ = optional_default(ghost_update, .true.)
  to_cartesian_ = optional_default(to_cartesian, .true.)

  if (to_cartesian_) then
    select type (coord_system => der%mesh%coord_system)
    type is (cartesian_t)
    class default
      ! Not implemented yet
      ASSERT(.false.)
    end select
  end if

  call X(derivatives_batch_perform)(der%grad(1), der, ffb(1), opffb, &
    ghost_update=ghost_update_, set_bc=set_bc_)

  call opffb%clone_to(tmpb)

  do idir = 2, der%dim
    call X(derivatives_batch_perform)(der%grad(idir), der, ffb(idir), tmpb, &
      ghost_update=ghost_update_, set_bc=set_bc_)

    call batch_axpy(der%mesh%np, M_ONE, tmpb, opffb)
  end do

  call tmpb%end()

  call profiling_out(TOSTRING(X(BATCH_DIVERGENCE)))
  POP_SUB(X(derivatives_batch_div))
end subroutine X(derivatives_batch_div)

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
