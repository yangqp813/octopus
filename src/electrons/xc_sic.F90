!! Copyright (C) 2002-2006 M. Marques, A. Castro, A. Rubio, G. Bertsch
!! Copyright (C) 2022 N. Tancogne-Dejean
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module xc_sic_oct_m
  use debug_oct_m
  use electron_space_oct_m
  use global_oct_m
  use grid_oct_m
  use hamiltonian_elec_oct_m
  use lalg_basic_oct_m
  use mesh_function_oct_m
  use messages_oct_m
  use multicomm_oct_m
  use namespace_oct_m
  use parser_oct_m
  use poisson_oct_m
  use profiling_oct_m
  use space_oct_m
  use states_elec_oct_m
  use states_elec_dim_oct_m
  use varinfo_oct_m
  use xc_oct_m
  use xc_f03_lib_m
  use xc_oep_oct_m

  implicit none

  private
  public ::                     &
    xc_sic_t,                   &
    xc_sic_init,                &
    xc_sic_calc_adsic,          &
    xc_sic_write_info,          &
    xc_sic_end

  !> the SIC levels
  integer, parameter, public :: &
    SIC_NONE   = 1,     &  !< no self-interaction correction
    SIC_PZ_OEP = 2,     &  !< Perdew-Zunger SIC (OEP way)
    SIC_AMALDI = 3,     &  !< Amaldi correction term
    SIC_ADSIC  = 4         !< Averaged density SIC

  !> This class contains information about the self-interaction correction
  type xc_sic_t
    private
    integer,        public :: level = SIC_NONE  !< what kind of self-interaction correction to apply
    real(real64),   public :: amaldi_factor     !< for Amaldi SIC scale the density by \f$(q_{tot}-1)/q_{tot}\f$
    type(xc_oep_t), public :: oep               !< The OEP class for solving the SIC equations
  end type xc_sic_t

contains

  ! ---------------------------------------------------------
  !> @brief initialize the SIC object
  !
  subroutine xc_sic_init(sic, namespace, gr, st, mc, space)
    type(xc_sic_t),      intent(out)   :: sic
    type(namespace_t),   intent(in)    :: namespace
    type(grid_t),        intent(inout) :: gr
    type(states_elec_t), intent(in)    :: st
    type(multicomm_t),   intent(in)    :: mc
    class(space_t),      intent(in)    :: space


    PUSH_SUB(xc_sic_init)

    !%Variable SICCorrection
    !%Type integer
    !%Default sic_none
    !%Section Hamiltonian::XC
    !%Description
    !% This variable controls which form of self-interaction correction to use. Note that
    !% this correction will be applied to the functional chosen by <tt>XCFunctional</tt>.
    !%Option sic_none 1
    !% No self-interaction correction.
    !%Option sic_pz 2
    !% Perdew-Zunger SIC, handled by the OEP technique.
    !% J. P. Perdew and Alex Zunger, Phys. Rev. B 23, 5048 (1981)
    !% Extension to the spinor case follows Tancogne-Dejean et al., J. Chem. Phys. 159, 224110 (2023)
    !%Option sic_amaldi 3
    !% Amaldi correction term. Not implemeneted for spinors.
    !% E. Fermi and E. Amaldi, Mem. Reale Accad. Italia 6, 119 (1934)
    !%Option sic_adsic 4
    !% Average-density SIC.
    !% C. Legrand <i>et al.</i>, <i>J. Phys. B</i> <b>35</b>, 1115 (2002).
    !% Extension to the spinor case follows Tancogne-Dejean et al., J. Chem. Phys. 159, 224110 (2023)
    !%End
    call parse_variable(namespace, 'SICCorrection', SIC_NONE, sic%level)
    if (.not. varinfo_valid_option('SICCorrection', sic%level)) call messages_input_error(namespace, 'SICCorrection')

    ! check whether we should introduce the Amaldi SIC correction
    sic%amaldi_factor = M_ONE
    if (sic%level == SIC_AMALDI) then
      sic%amaldi_factor = (st%qtot - M_ONE)/st%qtot
      if(st%d%ispin == SPINORS) then
        call messages_not_implemented("Amaldi SIC with non-collinear spins")
      end if
    end if

    if(sic%level == SIC_PZ_OEP) then
      call xc_oep_init(sic%oep, namespace, gr, st, mc, space, oep_type = OEP_TYPE_SIC)

      if(st%nik > st%d%spin_channels) then
        call messages_not_implemented("PZ-SIC with k-points")
      end if
    end if

    if (allocated(st%rho_core)) then
      call messages_not_implemented('SIC with nonlinear core corrections')
    end if

    if (allocated(st%frozen_rho) .and. (sic%level == SIC_PZ_OEP .or. sic%level == SIC_AMALDI)) then
      call messages_not_implemented('PZ-SIC with frozen orbitals')
    end if

    POP_SUB(xc_sic_init)
  end subroutine xc_sic_init

  ! ---------------------------------------------------------
  !> @brief finalize the SIC and, if needed, the included OEP
  subroutine xc_sic_end(sic)
    type(xc_sic_t),  intent(inout) :: sic

    if (sic%level == SIC_NONE) return

    PUSH_SUB(xc_sic_end)

    if(sic%level == SIC_PZ_OEP) call xc_oep_end(sic%oep)

    POP_SUB(xc_sic_end)
  end subroutine xc_sic_end


  ! ---------------------------------------------------------
  subroutine xc_sic_write_info(sic, iunit, namespace)
    type(xc_sic_t),              intent(in) :: sic
    integer,           optional, intent(in) :: iunit
    type(namespace_t), optional, intent(in) :: namespace

    if (sic%level == SIC_NONE) return

    PUSH_SUB(xc_sic_write_info)

    call messages_print_var_option('SICCorrection', sic%level, iunit=iunit, namespace=namespace)

    POP_SUB(xc_sic_write_info)
  end subroutine xc_sic_write_info

  ! ---------------------------------------------------------
  !> @brief Computes the ADSIC potential and energy
  !!
  !! ADSIC potential is:
  !! \f[
  !! V_ADSIC[n] = V_ks[n] - (V_h[n/N] + V_xc[n_{up}/N_{up},0] + Vxc(0, n_{dn}/N_{dn}))
  !! \f]
  !!
  !! \f[
  !! E_ADSIC[n] = E - [N E_H[n/N] + N_{up} E_xc[n_{up}/N_{up},0] + N_{dn} Exc(0, n_{dn}/N_{dn})
  !! \f]
  !!
  !! C. Legrand et al., J. Phys. B: At. Mol. Opt. Phys. 35 (2002) 1115–1128
  !!
  !! @note The Hartree term is wrong in the above reference, see
  !! See Eq. 15 in [Pietezak and Vieira, Theoretical Chemistry Accounts (2021) 140:130] instead
  !! or Eq. 38 in the Perdew-Zunger paper.
  subroutine xc_sic_calc_adsic(sic, namespace, space, gr, st, hm, xc, density, vxc, ex, ec)
    type(xc_sic_t),              intent(in) :: sic
    type(namespace_t),           intent(in) :: namespace
    class(space_t),              intent(in) :: space
    type(grid_t),                intent(in) :: gr
    type(states_elec_t),         intent(in) :: st
    type(hamiltonian_elec_t),    intent(in) :: hm
    type(xc_t),               intent(inout) :: xc
    real(real64), contiguous,           intent(in) :: density(:,:)
    real(real64), contiguous,        intent(inout) :: vxc(:,:)
    real(real64), optional,          intent(inout) :: ex, ec

    integer            :: ispin, ist, ik, ip
    real(real64), allocatable :: vxc_sic(:,:),  vh_sic(:), rho(:, :), qsp(:)
    real(real64) :: ex_sic, ec_sic
    real(real64) :: dtot, dpol, vpol
    real(real64), parameter :: tiny = 1e-12_real64 !< This is the same as in xc.F90. Please keep consistent.
    real(real64)  :: nup

    PUSH_SUB(xc_sic_calc_adsic)

    ASSERT(sic%level == SIC_ADSIC)

    if (st%d%ispin == SPINORS .and. bitand(xc%family, XC_FAMILY_LDA) == 0) then
      call messages_not_implemented('ADSIC with non-collinear spin and GGAs', namespace=namespace)
    end if

    ! We compute here the number of electrons per spin channel
    SAFE_ALLOCATE(qsp(1:2))
    qsp = M_ZERO
    if( .not. allocated(st%frozen_rho)) then
      select case (st%d%ispin)
      case (UNPOLARIZED, SPIN_POLARIZED)
        do ist = 1, st%nst
          do ik = 1, st%nik
            ispin = st%d%get_spin_index(ik)
            qsp(ispin) = qsp(ispin) + st%occ(ist, ik) * st%kweights(ik)
          end do
        end do
      end select
    else
      ! In the case of the frozen density, we can only get the charge from the integral
      ! of the total density, including valence and frozen density
      do ispin = 1, st%d%nspin
        qsp(ispin) = dmf_integrate(gr, density(:, ispin))
      end do
    end if

    SAFE_ALLOCATE(vxc_sic(1:gr%np, 1:2))
    SAFE_ALLOCATE(vh_sic(1:gr%np))
    SAFE_ALLOCATE(rho(1:gr%np, 1:2))
    ! We first compute the average xc self-interction error and we substract it
    select case (st%d%ispin)
    case (UNPOLARIZED, SPIN_POLARIZED)
      do ispin = 1, st%d%nspin
        if (abs(qsp(ispin)) <= M_MIN_OCC) cycle

        rho = M_ZERO
        vxc_sic = M_ZERO

        rho(:, ispin) = density(:, ispin) / qsp(ispin)
        if(present(ex) .and. present(ec)) then
          ex_sic = M_ZERO
          ec_sic = M_ZERO
          ! This needs always to be called for the spin-polarized case
          call xc_get_vxc(gr, xc, st, hm%kpoints, hm%psolver, namespace, space, &
            rho, SPIN_POLARIZED, hm%ions%latt%rcell_volume, vxc_sic, ex = ex_sic, ec = ec_sic)
          ex = ex - ex_sic * qsp(ispin)
          ec = ec - ec_sic * qsp(ispin)
        else
          ! This needs always to be called for the spin-polarized case
          call xc_get_vxc(gr, xc, st, hm%kpoints, hm%psolver, namespace, space, &
            rho, SPIN_POLARIZED, hm%ions%latt%rcell_volume, vxc_sic)
        end if

        call lalg_axpy(gr%np, -M_ONE, vxc_sic(:, ispin), vxc(:, ispin))

        ! We now substract the averaged Hartree self-interaction error
        ! See Eq. 15 in [Pietezak and Vieira, Theoretical Chemistry Accounts (2021) 140:130]
        vh_sic = M_ZERO
        call dpoisson_solve(hm%psolver, namespace, vh_sic, rho(:, ispin), all_nodes=.false.)
        call lalg_axpy(gr%np, -M_ONE, vh_sic, vxc(:, ispin))

        ! Compute the corresponding energy contribution
        if(present(ex)) then
          ex = ex - M_HALF*dmf_dotp(gr, rho(:,ispin), vh_sic) * qsp(ispin)
        end if

      end do

    case (SPINORS)
      ! Here we only treat the case of LDA. We rotate the average density in the local frame
      ! And we then compute the SIC correction from it
      ! This cannot excerce any xc torque, by construction
      ASSERT(bitand(xc%family, XC_FAMILY_LDA) /= 0)

      do ispin = 1, 2
        rho = M_ZERO
        vxc_sic = M_ZERO
        ! Averaged density in the local frame
        do ip = 1, gr%np
          dtot = density(ip, 1) + density(ip, 2)
          dpol = sqrt((density(ip, 1) - density(ip, 2))**2 + &
            M_FOUR*(density(ip, 3)**2 + density(ip, 4)**2))
          if(ispin == 1) then
            rho(ip, 1) = max(M_HALF*(dtot + dpol), M_ZERO)
          else
            rho(ip, 2) = max(M_HALF*(dtot - dpol), M_ZERO)
          end if
        end do
        nup = dmf_integrate(gr, rho(:,ispin))
        if (nup <= 1e-14_real64) cycle
        call lalg_scal(gr%np, M_ONE/nup, rho(:,ispin))

        ! This needs always to be called for the spin-polarized case
        if(present(ex) .and. present(ec)) then
          ex_sic = M_ZERO
          ec_sic = M_ZERO
          call xc_get_vxc(gr, xc, st, hm%kpoints, hm%psolver, namespace, space, &
            rho, SPIN_POLARIZED, hm%ions%latt%rcell_volume, vxc_sic, ex = ex_sic, ec = ec_sic)
          ex = ex - ex_sic * nup
          ec = ec - ec_sic * nup
        else
          call xc_get_vxc(gr, xc, st, hm%kpoints, hm%psolver, namespace, space, &
            rho, SPIN_POLARIZED, hm%ions%latt%rcell_volume, vxc_sic)
        end if

        ! Select only the potential correspond to this spin channel
        if(ispin == 2) then
          vxc_sic(:, 1) = M_ZERO
        else
          vxc_sic(:, 2) = M_ZERO
        end if

        vh_sic = M_ZERO
        call dpoisson_solve(hm%psolver, namespace, vh_sic, rho(:, ispin), all_nodes=.false.)
        call lalg_axpy(gr%np, M_ONE, vh_sic, vxc_sic(:, ispin))
        ! Compute the corresponding energy contribution
        if(present(ex)) then
          ex = ex - M_HALF*dmf_dotp(gr, rho(:,ispin), vh_sic) * nup
        end if

        do ip = 1, gr%np
          dpol = sqrt((density(ip, 1) - density(ip, 2))**2 + &
            M_FOUR*(density(ip, 3)**2 + density(ip, 4)**2))
          vpol = (vxc_sic(ip, 1) - vxc_sic(ip, 2))*(density(ip, 1) - density(ip, 2))/(SAFE_TOL(dpol, tiny))

          vxc(ip, 1) = vxc(ip, 1) - M_HALF*(vxc_sic(ip, 1) + vxc_sic(ip, 2) + vpol)
          vxc(ip, 2) = vxc(ip, 2) - M_HALF*(vxc_sic(ip, 1) + vxc_sic(ip, 2) - vpol)
          vxc(ip, 3) = vxc(ip, 3) - (vxc_sic(ip, 1) - vxc_sic(ip, 2))*density(ip, 3)/(SAFE_TOL(dpol, tiny))
          vxc(ip, 4) = vxc(ip, 4) - (vxc_sic(ip, 1) - vxc_sic(ip, 2))*density(ip, 4)/(SAFE_TOL(dpol, tiny))
        end do
      end do


    end select

    SAFE_DEALLOCATE_A(qsp)
    SAFE_DEALLOCATE_A(vxc_sic)
    SAFE_DEALLOCATE_A(vh_sic)
    SAFE_DEALLOCATE_A(rho)

    POP_SUB(xc_sic_calc_adsic)
  end subroutine xc_sic_calc_adsic

end module xc_sic_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
