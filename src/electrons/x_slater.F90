!! Copyright (C) 2002-2006 M. Marques, A. Castro, A. Rubio, G. Bertsch
!! Copyright (C) 2023 N. Tancogne-Dejean
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module x_slater_oct_m
  use comm_oct_m
  use debug_oct_m
  use electron_space_oct_m
  use exchange_operator_oct_m
  use global_oct_m
  use grid_oct_m
  use, intrinsic :: iso_fortran_env
  use kpoints_oct_m
  use lalg_basic_oct_m
  use mesh_function_oct_m
  use messages_oct_m
  use namespace_oct_m
  use profiling_oct_m
  use space_oct_m
  use states_abst_oct_m
  use states_elec_oct_m
  use states_elec_dim_oct_m

  implicit none

  private
  public ::               &
    x_slater_calc

contains

  ! ---------------------------------------------------------
  !>@brief Interface to X(slater_calc)
  subroutine x_slater_calc (namespace, gr, space, exxop, st, kpoints, ex, vxc)
    type(namespace_t),           intent(in)    :: namespace
    type(grid_t),                intent(in)    :: gr
    class(space_t),              intent(in)    :: space
    type(exchange_operator_t),   intent(in)    :: exxop
    type(states_elec_t),         intent(inout) :: st
    type(kpoints_t),             intent(in)    :: kpoints
    real(real64),                intent(inout) :: ex
    real(real64), optional,             intent(inout) :: vxc(:,:) !< vxc(gr%mesh%np, st%d%nspin)

    PUSH_SUB(x_slater_calc)

    if (states_are_real(st)) then
      call dslater_calc(namespace, gr, space, exxop, st, kpoints, ex, vxc)
    else
      call zslater_calc(namespace, gr, space, exxop, st, kpoints, ex, vxc)
    end if

    POP_SUB(x_slater_calc)
  end subroutine x_slater_calc

  ! ---------------------------------------------------------
  !>@brief This routine get the SU(2) matrix that diagonalize the spin-density matrix
  !!
  !! The matrix has the following form
  !!
  !!\f[
  !!\begin{pmatrix}
  !! \alpha & \beta \\
  !! -\beta^* & \alpha\\
  !!\end{pmatrix}
  !!\f]
  !!
  !! where \f$\alpha\f$ is real.
  subroutine get_rotation_matrix(dens, alpha, betar, betai)
    real(real64),  intent(in)  :: dens(:) !< spin-density matrix
    real(real64),  intent(out) :: alpha, betar, betai !< Coefficients of the matrix

    real(real64), parameter :: tiny = 1e-12_real64
    real(real64) :: mz, mm

    mz = dens(1) - dens(2)

    mm = sqrt(mz**2 + M_FOUR*(dens(3)**2 + dens(4)**2))

    !Fully spin unpolarized system
    if (mm < tiny) then
      alpha = M_ONE
      betar = M_ZERO
      betai = M_ZERO
      return
    end if

    alpha = sqrt((mm + abs(mz))/(M_TWO * mm))
    !We find the absolute values of real and imaginary parts of beta
    betar = M_TWO * dens(3) / sqrt(M_TWO * mm * (mm + abs(mz)))
    betai = M_TWO * dens(4) / sqrt(M_TWO * mm * (mm + abs(mz)))

    if (mz < M_ZERO) then
      betar = -betar
      betai = -betai
    end if

  end subroutine get_rotation_matrix

  ! ---------------------------------------------------------
  !>@brief Rotation to the local frame
  !! Given a matrix in spin space, this routine rotates according to the rotation
  !! matrix R defined by the alpha and beta coefficients
  !! \f$D = R M R^T\f$
  subroutine rotate_to_local(mat, alpha, betar, betai, alpha2, beta2, rot_mat)
    real(real64),  intent(in)  :: mat(:)
    real(real64),  intent(in)  :: alpha, betar, betai, alpha2, beta2
    real(real64),  intent(out) :: rot_mat(:)

    complex(real64) :: cross

    rot_mat(1) = alpha2 * mat(1) + beta2 * mat(2) + M_TWO * alpha * (betar * mat(3) + betai * mat(4))
    rot_mat(2) = alpha2 * mat(2) + beta2 * mat(1) - M_TWO * alpha * (betar * mat(3) + betai * mat(4))
    cross = (cmplx(betar, betai, real64) )**2 * cmplx(mat(3), -mat(4), real64)
    rot_mat(3) = alpha2 * mat(3) + alpha * betar * (mat(2)-mat(1)) - real(cross)
    rot_mat(4) = alpha2 * mat(4) + alpha * betai * (mat(2)-mat(1)) - aimag(cross)

  end subroutine rotate_to_local

  ! ---------------------------------------------------------
  !>@brief Rotation to the global (Cartesian) frame
  !!
  !! Given a matrix in spin space, this routine rotates according to the rotation
  !! matrix R defined by the alpha and beta coefficients
  !! \f$M = R^T D R\f$
  subroutine rotate_to_global(mat, alpha, betar, betai, alpha2, beta2, rot_mat)
    real(real64),  intent(in)  :: mat(:)
    real(real64),  intent(in)  :: alpha, betar, betai, alpha2, beta2
    real(real64),  intent(out) :: rot_mat(:)

    complex(real64) :: cross

    rot_mat(1) = alpha2 * mat(1) + beta2 * mat(2) - M_TWO * alpha * (betar * mat(3) + betai * mat(4))
    rot_mat(2) = alpha2 * mat(2) + beta2 * mat(1) + M_TWO * alpha * (betar * mat(3) + betai * mat(4))
    cross = (cmplx(betar, betai, real64) )**2 * cmplx(mat(3), -mat(4), real64)
    rot_mat(3) = alpha2 * mat(3) - alpha * betar * (mat(2)-mat(1)) - real(cross)
    rot_mat(4) = alpha2 * mat(4) - alpha * betai * (mat(2)-mat(1)) - aimag(cross)

  end subroutine rotate_to_global


#include "undef.F90"
#include "real.F90"
#include "x_slater_inc.F90"

#include "undef.F90"
#include "complex.F90"
#include "x_slater_inc.F90"

end module x_slater_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
