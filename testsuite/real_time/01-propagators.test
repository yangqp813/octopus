# -*- coding: utf-8 mode: shell-script -*-

Test       : Propagators
Program    : octopus
TestGroups : short-run, real_time
Enabled    : Yes

#This test checks the time propagation, for various propagators

# ground state
Processors : 1
Input      : 01-propagators.01-gs.inp
Precision: 2e-08
match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1
Precision: 5.30e-08
match ;   Initial energy   ; GREPFIELD(static/info, 'Total       =', 3) ; -10.60686609

Processors : 4
Input      : 01-propagators.02-expmid.inp
Precision: 5.13e-15
match ;   Forces [step  1]   ; LINEFIELD(td.general/coordinates, -21, 15) ; 0.08537673799433354
Precision: 4.15e-15
match ;   Forces [step 20]   ; LINEFIELD(td.general/coordinates, -1, 15) ; 0.07966840852244794
Precision: 1.06e-13
match ;  Energy [step  1]  ; LINEFIELD(td.general/energy, -21, 3) ; -10.606866087667619
Precision: 1.06e-13
match ;  Energy [step 20]  ; LINEFIELD(td.general/energy, -1, 3) ; -10.606373536664119
Precision: 4.49e-15
match ;  Multipoles [step  1]  ; LINEFIELD(td.general/multipoles, -21, 4) ; 1.824331091466839e-16
Precision: 2.08e-15
match ;  Multipoles [step 20]  ; LINEFIELD(td.general/multipoles, -1, 4) ; -0.1265509663990613

Processors : 4
Input      : 01-propagators.03-etrs_taylor.inp
Precision: 1.06e-13
match ;  Energy [step  0]  ; LINEFIELD(td.general/energy, -21, 3) ; -10.606866087667619
Precision: 1.06e-13
match ;  Energy [step 20]  ; LINEFIELD(td.general/energy, -1, 3) ; -10.606372053182291
Precision: 4.49e-15
match ;  Multipoles [step  0]  ; LINEFIELD(td.general/multipoles, -21, 4) ; 1.824331091466839e-16
Precision: 3.51e-15
match ;  Multipoles [step 20]  ; LINEFIELD(td.general/multipoles, -1, 4) ; -0.1265536606512522
Precision: 5.13e-15
match ;  Forces [step  0]   ; LINEFIELD(td.general/coordinates, -21, 15) ; 0.08537673799433354
Precision: 2.44e-15
match ;  Forces [step 20]   ; LINEFIELD(td.general/coordinates, -1, 15) ; 0.07966960326023464

Processors : 4
Input      : 01-propagators.04-etrs_lanczos.inp
Precision: 1.06e-13
match ;  Energy [step  0]  ; LINEFIELD(td.general/energy, -21, 3) ; -10.606866087667619
Precision: 1.06e-13
match ;  Energy [step 20]  ; LINEFIELD(td.general/energy, -1, 3) ; -10.606372005969709
Precision: 4.49e-15
match ;  Multipoles [step  0]  ; LINEFIELD(td.general/multipoles, -21, 4) ; 1.824331091466839e-16
Precision: 4.36e-14
match ;  Multipoles [step 20]  ; LINEFIELD(td.general/multipoles, -1, 4) ; -0.1265864295118192
Precision: 5.13e-15
match ;  Forces [step  0]   ; LINEFIELD(td.general/coordinates, -21, 15) ; 0.08537673799433354
Precision: 4.64e-14
match ;  Forces [step 20]   ; LINEFIELD(td.general/coordinates, -1, 15) ; 0.07967423673803031

Processors : 4
Input      : 01-propagators.05-etrs_chebyshev.inp
Precision: 1.06e-13
match ;  Energy [step  0]  ; LINEFIELD(td.general/energy, -21, 3) ; -10.606866087667619
Precision: 1.06e-13
match ;  Energy [step 20]  ; LINEFIELD(td.general/energy, -1, 3) ; -10.59306110576122
Precision: 4.49e-15
match ;  Multipoles [step  0]  ; LINEFIELD(td.general/multipoles, -21, 4) ; 1.824331091466839e-16
Precision: 3.85e-15
match ;  Multipoles [step 20]  ; LINEFIELD(td.general/multipoles, -1, 4) ; -0.1269354346362595
Precision: 5.13e-15
match ;  Forces [step  0]   ; LINEFIELD(td.general/coordinates, -21, 15) ; 0.08537673799433354
Precision: 3.86e-15
match ;  Forces [step 20]   ; LINEFIELD(td.general/coordinates, -1, 15) ; 0.0771329923764783

Processors : 4
Input      : 01-propagators.06-aetrs.inp
Precision: 1.06e-13
match ;  Energy [step  0]  ; LINEFIELD(td.general/energy, -21, 3) ; -10.606866087667619
Precision: 1.06e-13
match ;  Energy [step 20]  ; LINEFIELD(td.general/energy, -1, 3) ; -10.606373536664119
Precision: 4.49e-15
match ;  Multipoles [step  0]  ; LINEFIELD(td.general/multipoles, -21, 4) ; 1.824331091466839e-16
Precision: 2.08e-15
match ;  Multipoles [step 20]  ; LINEFIELD(td.general/multipoles, -1, 4) ; -0.1265509663990613
Precision: 5.13e-15
match ;  Forces [step  0]   ; LINEFIELD(td.general/coordinates, -21, 15) ; 0.08537673799433354
Precision: 4.15e-15
match ;  Forces [step 20]   ; LINEFIELD(td.general/coordinates, -1, 15) ; 0.07966840852244794

Processors : 4
Input      : 01-propagators.07-caetrs.inp
Precision: 1.06e-13
match ;  Energy [step  0]  ; LINEFIELD(td.general/energy, -21, 3) ; -10.606866087667619
Precision: 1.06e-13
match ;  Energy [step 20]  ; LINEFIELD(td.general/energy, -1, 3) ; -10.60647752896755
Precision: 4.49e-15
match ;  Multipoles [step  0]  ; LINEFIELD(td.general/multipoles, -21, 4) ; 1.824331091466839e-16
Precision: 2.77e-15
match ;  Multipoles [step 20]  ; LINEFIELD(td.general/multipoles, -1, 4) ; -0.1108691829653119

Processors : 4
Input      : 01-propagators.08-cn.inp
Precision: 1.06e-13
match ;  Energy [step  0]  ; LINEFIELD(td.general/energy, -21, 3) ; -10.606866087667619
Precision: 1.06e-13
match ;  Energy [step 20]  ; LINEFIELD(td.general/energy, -1, 3) ; -10.60637259622256
Precision: 4.49e-15
match ;  Multipoles [step  0]  ; LINEFIELD(td.general/multipoles, -21, 4) ; 1.824331091466839e-16
Precision: 2.56e-13
match ;  Multipoles [step 20]  ; LINEFIELD(td.general/multipoles, -1, 4) ; -0.1265304356373742
Precision: 5.13e-15
match ;  Forces [step  0]   ; LINEFIELD(td.general/coordinates, -21, 15) ; 0.08537673799433354
Precision: 1.23e-12
match ;  Forces [step 20]   ; LINEFIELD(td.general/coordinates, -1, 15) ; 0.07964956569127124

Processors : 4
Input      : 01-propagators.09-magnus.inp
Precision: 1.06e-13
match ;  Energy [step  0]  ; LINEFIELD(td.general/energy, -21, 3) ; -10.606866087667619
Precision: 1.06e-13
match ;  Energy [step 20]  ; LINEFIELD(td.general/energy, -1, 3) ; -10.6064783204069
Precision: 4.49e-15
match ;  Multipoles [step  0]  ; LINEFIELD(td.general/multipoles, -21, 4) ; 1.824331091466839e-16
Precision: 2.70e-15
match ;  Multipoles [step 20]  ; LINEFIELD(td.general/multipoles, -1, 4) ; -0.110859710205229

Processors : 4
Input      : 01-propagators.10-exprk4.inp
Precision: 1.06e-13
match ;  Energy [step  0]  ; LINEFIELD(td.general/energy, -21, 3) ; -10.606866087667619
Precision: 1.06e-13
match ;  Energy [step 20]  ; LINEFIELD(td.general/energy, -1, 3) ; -10.60647930997464
Precision: 4.49e-15
match ;  Multipoles [step  0]  ; LINEFIELD(td.general/multipoles, -21, 4) ; 1.824331091466839e-16
Precision: 2.67e-15
match ;  Multipoles [step 20]  ; LINEFIELD(td.general/multipoles, -1, 4) ; -0.1108447871283799

Processors : 4
Input      : 01-propagators.11-cfmagnus4.inp
Precision: 1.06e-13
match ;  Energy [step  0]  ; LINEFIELD(td.general/energy, -21, 3) ; -10.606866087667619
Precision: 1.06e-13
match ;  Energy [step 20]  ; LINEFIELD(td.general/energy, -1, 3) ; -10.60647789332938
Precision: 4.49e-15
match ;  Multipoles [step  0]  ; LINEFIELD(td.general/multipoles, -21, 4) ; 1.824331091466839e-16
Precision: 3.19e-15
match ;  Multipoles [step 20]  ; LINEFIELD(td.general/multipoles, -1, 4) ; -0.1108452722193976