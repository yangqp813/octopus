# -*- coding: utf-8 mode: shell-script -*-

Test       : Crank-Nicolson
Program    : octopus
TestGroups : short-run, real_time
Enabled    : Yes

#This test checks the time propagation

# ground state
Processors : 1
Input      : 15-crank_nicolson.01-gs.inp
match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1
Precision: 5.30e-08
match ;  Initial energy  ; GREPFIELD(static/info, 'Total       =', 3) ; -10.60686763

Processors : 4
Input      : 15-crank_nicolson.02-kick.inp
Precision: 1.06e-13
match ;  Energy [step  1]  ; LINEFIELD(td.general/energy, -21, 3) ; -10.58497392618078
Precision: 1.04e-13
match ;  Energy [step  5]  ; LINEFIELD(td.general/energy, -16, 3) ; -10.429524107669689
Precision: 1.04e-13
match ;  Energy [step 10]  ; LINEFIELD(td.general/energy, -11, 3) ; -10.42951819589651
Precision: 1.04e-13
match ;  Energy [step 15]  ; LINEFIELD(td.general/energy, -6, 3) ; -10.42951645116274
Precision: 1.13e-13
match ;  Energy [step 20]  ; LINEFIELD(td.general/energy, -1, 3) ; -10.42951647298858

Precision: 6.52e-15
match ;  Dipole [step  1]  ; LINEFIELD(td.general/multipoles, -21, 4) ; 1.78063811661015e-16
Precision: 3.93e-13
match ;  Dipole [step  5]  ; LINEFIELD(td.general/multipoles, -16, 4) ; -0.72953696870934
Precision: 8.65e-13
match ;  Dipole [step 10]  ; LINEFIELD(td.general/multipoles, -11, 4) ; -1.3392629790003319
Precision: 8.39e-13
match ;  Dipole [step 15]  ; LINEFIELD(td.general/multipoles, -6, 4) ; -1.8338282394161889
Precision: 2.80e-13
match ;  Dipole [step 20]  ; LINEFIELD(td.general/multipoles, -1, 4) ; -2.2152994458257282

Input      : 15-crank_nicolson.03-freeze_domains.inp
Precision: 1.02e-13
match ;  Energy [step  1]  ; LINEFIELD(td.general/energy, -21, 3) ; -10.22657179873567
Precision: 1.01e-13
match ;  Energy [step  5]  ; LINEFIELD(td.general/energy, -16, 3) ; -10.14067705119932
Precision: 1.01e-13
match ;  Energy [step 10]  ; LINEFIELD(td.general/energy, -11, 3) ; -10.13381790798763
Precision: 1.01e-13
match ;  Energy [step 15]  ; LINEFIELD(td.general/energy, -6, 3) ; -10.12563491015765
Precision: 1.01e-13
match ;  Energy [step 20]  ; LINEFIELD(td.general/energy, -1, 3) ; -10.11739812323845


Precision: 8.00e-15
match ;   Dipole [step  1]   ; LINEFIELD(td.general/multipoles, -21, 4) ; -7.216449660063501e-16
Precision: 9.28e-15
match ;   Dipole [step  5]   ; LINEFIELD(td.general/multipoles, -16, 4) ; -0.3955586853259932
Precision: 7.41e-15
match ;  Dipole [step 10]  ; LINEFIELD(td.general/multipoles, -11, 4) ; -0.7406750729061549
Precision: 1.94e-14
match ;  Dipole [step 15]  ; LINEFIELD(td.general/multipoles, -6, 4) ; -1.039084057464608
Precision: 1.49e-14
match ;  Dipole [step 20]  ; LINEFIELD(td.general/multipoles, -1, 4) ; -1.29582993653278

Precision: 3.61e-14
match ;  Projections  ; LINEFIELD(td.general/projections, -1, 3) ; 1.1796119636642288e-16
Precision: 3.94e-14
match ;  Projections  ; LINEFIELD(td.general/projections, -1, 7) ; 1

#Values below are copied from 15-crank_nicolson.03-freeze_domains.inp
Input      : 15-crank_nicolson.04-freeze_states.inp

Precision: 1.02e-13
match ;  Energy [step  1]  ; LINEFIELD(td.general/energy, -21, 3) ; -10.22657179873567
Precision: 1.01e-13
match ;  Energy [step  5]  ; LINEFIELD(td.general/energy, -16, 3) ; -10.14067705119932
Precision: 1.01e-13
match ;  Energy [step 10]  ; LINEFIELD(td.general/energy, -11, 3) ; -10.13381790798763
Precision: 1.01e-13
match ;  Energy [step 15]  ; LINEFIELD(td.general/energy, -6, 3) ; -10.12563491015765
Precision: 1.01e-13
match ;  Energy [step 20]  ; LINEFIELD(td.general/energy, -1, 3) ; -10.11739812323845

Precision: 5.83e-15
match ;   Dipole [step  1]   ; LINEFIELD(td.general/multipoles, -21, 4) ; -1.03910839486946e-15
Precision: 6.11e-15
match ;  Dipole [step  5]  ; LINEFIELD(td.general/multipoles, -16, 4) ; -0.3955586853259905
Precision: 7.41e-15
match ;  Dipole [step 10]  ; LINEFIELD(td.general/multipoles, -11, 4) ; -0.7406750729061549
Precision: 1.14e-14
match ;  Dipole [step 15]  ; LINEFIELD(td.general/multipoles, -6, 4) ; -1.039084057464601
Precision: 1.30e-14
match ;  Dipole [step 20]  ; LINEFIELD(td.general/multipoles, -1, 4) ; -1.295829936532786


Processors : 1
Input : 15-crank_nicolson.05-freeze_sae.inp
Precision: 2.39e-14
match ;   Energy [step  1]   ; LINEFIELD(td.general/energy, -21, 3) ; -0.557193825989401
Precision: 2.49e-14
match ;   Energy [step  5]   ; LINEFIELD(td.general/energy, -16, 3) ; -0.5157327347418511
Precision: 3.03e-14
match ;   Energy [step 10]   ; LINEFIELD(td.general/energy, -11, 3) ; -0.5157327347416979
Precision: 2.78e-14
match ;   Energy [step 15]   ; LINEFIELD(td.general/energy, -6, 3) ; -0.5157327347416708
Precision: 4.10e-14
match ;   Energy [step 20]   ; LINEFIELD(td.general/energy, -1, 3) ; -0.5157327347416979
Precision: 5.56e-15
match ;   Dipole [step  1]   ; LINEFIELD(td.general/multipoles, -21, 4) ; -7.379220068245151e-16
Precision: 4.59e-15
match ;   Dipole [step  5]   ; LINEFIELD(td.general/multipoles, -16, 4) ; -0.1928247888208892
Precision: 4.43e-15
match ;   Dipole [step 10]   ; LINEFIELD(td.general/multipoles, -11, 4) ; -0.3545495747886674
Precision: 4.95e-15
match ;   Dipole [step 15]   ; LINEFIELD(td.general/multipoles, -6, 4) ; -0.4859689858458459
Precision: 6.96e-15
match ;   Dipole [step 20]   ; LINEFIELD(td.general/multipoles, -1, 4) ; -0.6087120870676892
