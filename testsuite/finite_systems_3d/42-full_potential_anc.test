# -*- coding: utf-8 mode: shell-script -*-

Test       : Analytical Norm-conserving potential for Hydrogen
Program    : octopus
TestGroups : finite_systems_3d, long-run
Enabled    : Yes

Input: 42-full_potential_anc.01-gs.inp
match ; SCF convergence    ; GREPCOUNT(static/info, 'SCF converged') ; 1

Precision: 2.53e-07
match ;   Total energy         ; GREPFIELD(static/info, 'Total       =', 3) ; -0.50579838
Precision: 1.00e-04
match ;   Ion-ion energy       ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; 0.0
Precision: 2.53e-07
match ;   Eigenvalues sum      ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -0.50579838
Precision: 1.00e-04
match ;   Hartree energy       ; GREPFIELD(static/info, 'Hartree     =', 3) ; 0.0
Precision: 2.60e-06
match ;   Kinetic energy       ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 0.5197918
Precision: 5.13e-08
match ;   External energy      ; GREPFIELD(static/info, 'External    =', 3) ; -1.02559019
Precision: 2.53e-06
match ;   Eigenvalue 1                    ; GREPFIELD(static/info, ' 1   -- ', 3) ; -0.505798
Precision: 4.43e-05
match ;    Eigenvalue 2                     ; GREPFIELD(static/info, ' 2   -- ', 3) ; -0.088525
Precision: 2.77e-05
match ;    Eigenvalue 3                     ; GREPFIELD(static/info, ' 3   -- ', 3) ; -0.055445

Input: 42-full_potential_anc.02-sc-zora.inp

Precision: 1.00e-01
match ;  SCF convergence     ; GREPCOUNT(static/info, 'SCF converged') ; 1.0

Precision: 2.53e-07
match ;    Total energy          ; GREPFIELD(static/info, 'Total       =', 3) ; -0.50584105
Precision: 1.00e-04
match ;    Ion-ion energy        ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; 0.0
Precision: 2.53e-07
match ;    Eigenvalues sum       ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -0.50584105
Precision: 1.00e-04
match ;    Hartree energy        ; GREPFIELD(static/info, 'Hartree     =', 3) ; 0.0
Precision: 2.60e-07
match ;    Kinetic energy        ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 0.51944491
Precision: 5.13e-08
match ;    External energy       ; GREPFIELD(static/info, 'External    =', 3) ; -1.02528596
Precision: 2.53e-05
match ;   Eigenvalue 1                    ; GREPFIELD(static/info, ' 1   -- ', 3) ; -0.505841
Precision: 4.59e-05
match ;   Eigenvalue 2                    ; GREPFIELD(static/info, ' 2   -- ', 3) ; -0.091703
Precision: 4.59e-05
match ;   Eigenvalue 3                    ; GREPFIELD(static/info, ' 3   -- ', 3) ; -0.091702

Input: 42-full_potential_anc.03-sf-zora.inp

Precision: 1.00e-01
match ;  SCF convergence     ; GREPCOUNT(static/info, 'SCF converged') ; 1.0

Precision: 5.06e-15
match ;    Total energy          ; GREPFIELD(static/info, 'Total       =', 3) ; -0.50584117
Precision: 1.00e-04
match ;    Ion-ion energy        ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; 0.0
Precision: 5.06e-15
match ;    Eigenvalues sum       ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -0.50584117
Precision: 1.00e-04
match ;    Hartree energy        ; GREPFIELD(static/info, 'Hartree     =', 3) ; 0.0
Precision: 2.60e-09
match ;    Kinetic energy        ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 0.51936805
Precision: 5.13e-08
match ;    External energy       ; GREPFIELD(static/info, 'External    =', 3) ; -1.02520922
Precision: 2.53e-05
match ;   Eigenvalue 1                    ; GREPFIELD(static/info, ' 1   -- ', 3) ; -0.505841
Precision: 2.53e-05
match ;   Eigenvalue 2                    ; GREPFIELD(static/info, ' 2   -- ', 3) ; -0.505841
Precision: 4.59e-05
match ;   Eigenvalue 3                    ; GREPFIELD(static/info, ' 3   -- ', 3) ; -0.091703

Input: 42-full_potential_anc.04-fr-zora.inp

Precision: 1.00e-01
match ;  SCF convergence     ; GREPCOUNT(static/info, 'SCF converged') ; 1.0

Precision: 2.53e-07
match ;    Total energy          ; GREPFIELD(static/info, 'Total       =', 3) ; -0.50584118
Precision: 1.00e-04
match ;    Ion-ion energy        ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; 0.0
Precision: 2.53e-07
match ;    Eigenvalues sum       ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -0.50584118
Precision: 1.00e-04
match ;    Hartree energy        ; GREPFIELD(static/info, 'Hartree     =', 3) ; 0.0
Precision: 2.60e-07
match ;    Kinetic energy        ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 0.51936943
Precision: 5.13e-07
match ;    External energy       ; GREPFIELD(static/info, 'External    =', 3) ; -1.0252105999999999

Precision: 2.53e-05
match ;   Eigenvalue 1                    ; GREPFIELD(static/info, ' 1   -- ', 3) ; -0.505841
Precision: 2.53e-05
match ;   Eigenvalue 2                    ; GREPFIELD(static/info, ' 2   -- ', 3) ; -0.505841
Precision: 4.58e-05
match ;   Eigenvalue 3                    ; GREPFIELD(static/info, ' 3   -- ', 3) ; -0.091699

Input: 42-full_potential_anc.05-zora-error.inp
match; Error;    GREPCOUNT(err, 'ZORA and vector potentials' ); 1
