# -*- coding: utf-8 mode: shell-script -*-

Test       : Helmholtz Decomposition
Program    : octopus
TestGroups : short-run, components
Enabled    : Yes

# Test Helmholtz without surface correction - We take a large box so that the surface correction contribution is negligible
# Note: For the Hertzian dipole case the norms are worse (and actually close to one) because the field has a singularity and the
# analytical reference is significantly non-zero at the boundaries
Processors : 2
Input      : 35-helmholtz_decom.01-large_box_no_surf_corr.inp
# Hertzian dipole, no surface correction, no Coulomb gauge
Precision: 2.50e-07
match ;  Transverse field test (rel.). ; LINEFIELD(hertzian_dipole_test/no_surf_corr/norms, 2, 6) ; 0.4513148918511
Precision: 2.50e-07
match ;  Longitudinal field test (rel.).  ; LINEFIELD(hertzian_dipole_test/no_surf_corr/norms, 4, 6) ; 0.9999999728505
Precision: 2.50e-07
match ;  Vector potential test (rel.). ; LINEFIELD(hertzian_dipole_test/no_surf_corr/norms, 6, 6) ; 14.7546493688641
Precision: 2.50e-07
match ;  Scalar potential test (rel.).  ; LINEFIELD(hertzian_dipole_test/no_surf_corr/norms, 8, 6) ; 0.9999999983515
Precision: 2.50e-07
match ;  Self consistency test (rel.).  ; LINEFIELD(hertzian_dipole_test/no_surf_corr/norms, 10, 6) ; 0.4516088548459
# Hertzian dipole, no surface correction, with Coulomb gauge for vector potential
Precision: 2.50e-07
match ;  Vector potential test (rel.). ; LINEFIELD(hertzian_dipole_test/vec_pot_from_b_field/norms, 6, 6) ; 15.5439847175009
# Gaussian field, no surface correction, no Coulomb gauge
Precision: 2.50e-07
match ;  Transverse field test (rel.). ; LINEFIELD(gaussian_field_test/no_surf_corr/norms, 2, 6) ; 0.0330178614356
Precision: 2.50e-07
match ;  Longitudinal field test (rel.).  ; LINEFIELD(gaussian_field_test/no_surf_corr/norms, 4, 6) ; 0.0190535965370
Precision: 2.50e-07
match ;  Vector potential test (rel.). ; LINEFIELD(gaussian_field_test/no_surf_corr/norms, 6, 6) ; 0.0103291706013
Precision: 2.50e-07
match ;  Scalar potential test (rel.).  ; LINEFIELD(gaussian_field_test/no_surf_corr/norms, 8, 6) ; 0.0039962839830
Precision: 2.50e-07
match ;  Self consistency test (rel.).  ; LINEFIELD(gaussian_field_test/no_surf_corr/norms, 10, 6) ; 0.0198874136160
# Gaussian field, no surface correction, with Coulomb gauge for vector potential
Precision: 5.0e-06
match ;  Vector potential test (rel.). ; LINEFIELD(gaussian_field_test/vec_pot_from_b_field/norms, 6, 6) ; 0.0089466330029

# PARALLELEPIPED Small box Coulomb gauge
Processors : 2
Input  : 35-helmholtz_decom.02-small_box_no_surf_corr.inp
# Hertzian dipole, no surface correction, no Coulomb gauge
Precision: 2.50e-07
match ;  Transverse field test (rel.). ; LINEFIELD(hertzian_dipole_test/no_surf_corr/norms, 2, 6) ; 0.3543133263198
Precision: 2.50e-07
match ;  Longitudinal field test (rel.).  ; LINEFIELD(hertzian_dipole_test/no_surf_corr/norms, 4, 6) ; 0.9999999728505
Precision: 2.50e-07
match ;  Vector potential test (rel.). ; LINEFIELD(hertzian_dipole_test/no_surf_corr/norms, 6, 6) ; 13.9243234064396
Precision: 2.50e-07
match ;  Scalar potential test (rel.).  ; LINEFIELD(hertzian_dipole_test/no_surf_corr/norms, 8, 6) ; 0.9999999983515
Precision: 2.50e-07
match ;  Self consistency test (rel.).  ; LINEFIELD(hertzian_dipole_test/no_surf_corr/norms, 10, 6) ; 0.3545667836500
# Hertzian dipole, no surface correction, with Coulomb gauge for vector potential
Precision: 2.50e-07
match ;  Vector potential test (rel.). ; LINEFIELD(hertzian_dipole_test/vec_pot_from_b_field/norms, 6, 6) ; 8.2659985772374
# Gaussian field, no surface correction, no Coulomb gauge
Precision: 2.50e-07
match ;  Transverse field test (rel.). ; LINEFIELD(gaussian_field_test/no_surf_corr/norms, 2, 6) ; 0.0850371795316
Precision: 2.50e-07
match ;  Longitudinal field test (rel.).  ; LINEFIELD(gaussian_field_test/no_surf_corr/norms, 4, 6) ; 0.0436006045975
Precision: 2.50e-07
match ;  Vector potential test (rel.). ; LINEFIELD(gaussian_field_test/no_surf_corr/norms, 6, 6) ; 0.3229322896418
Precision: 2.50e-07
match ;  Scalar potential test (rel.).  ; LINEFIELD(gaussian_field_test/no_surf_corr/norms, 8, 6) ; 0.6106597474968
Precision: 2.50e-07
match ;  Self consistency test (rel.).  ; LINEFIELD(gaussian_field_test/no_surf_corr/norms, 10, 6) ; 0.0578706671196
# Gaussian field, no surface correction, with Coulomb gauge for vector potential
Precision: 2.50e-07
match ;  Vector potential test (rel.). ; LINEFIELD(gaussian_field_test/vec_pot_from_b_field/norms, 6, 6) ; 0.0447778794168


# Test surface correction - We take a small box and we test that the deviations we get with the surface correction are smaller
# We do not have testcases because there are asserts in the code
# PARALLELEPIPED
Processors : 1
FailingInput  : 35-helmholtz_decom.03-parallelepiped_small_box_with_surf_corr.inp

match ; Error message ; GREPCOUNT(err, 'Surface correction for Helmholtz decomposition') ; 1

# SPHERE
Processors : 1
FailingInput  : 35-helmholtz_decom.04-cylinder_small_box_with_surf_corr.inp

match ; Error message ; GREPCOUNT(err, 'Surface correction for Helmholtz decomposition') ; 1

# CYLINDER
Processors : 1
FailingInput  : 35-helmholtz_decom.05-sphere_small_box_with_surf_corr.inp

match ; Error message ; GREPCOUNT(err, 'Surface correction for Helmholtz decomposition') ; 1

