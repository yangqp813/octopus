# -*- coding: utf-8 mode: shell-script -*-

Test       : Free Maxwell propagation through a linear medium
Program    : octopus
TestGroups : long-run, maxwell
Enabled    : no-GPU


# Cosinoidal pulse polarized in z-direction passing through medium box
Input      : 03-linear-medium.01-cosinoidal_pulse_td.inp

Precision: 2.75e-13
match ;     Tot. Maxwell energy [step 10]     ; LINEFIELD(Maxwell/td.general/maxwell_energy, 16, 3) ; 27.5297110779707
Precision: 2.02e-12
match ;     Tot. Maxwell energy [step 30]     ; LINEFIELD(Maxwell/td.general/maxwell_energy, 36, 3) ; 157.7142640682865
Precision: 1.73e-17
match ;     Ex  (x=  0,y=  0,z=-10) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-x\.x=0\,y=0, 20, 2) ; 0.000346590604487091
Precision: 1.73e-17
match ;     Ex  (x=  0,y=  0,z= 10) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-x\.x=0\,y=0, 40, 2) ; -0.00034659060448709203
Precision: 1.00e-16
match ;     Ex  (x=  0,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-x\.x=0\,y=0, 30, 2) ; 0.0
Precision: 1.0e-17
match ;     Ez  (x=-10,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.y=0\,z=0, 20, 2) ; 2.82567550955023e-05
Precision: 6.09e-15
match ;     Ez  (x=-10,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.y=0\,z=0, 20, 2) ; -0.121820135086794
Precision: 1.0e-17
match ;     Ez  (x= 10,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.y=0\,z=0, 40, 2) ; -1.3447934955325601e-06
Precision: 1.0e-17
match ;     Ez  (x=  0,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.y=0\,z=0, 30, 2) ; 5.85653822848294e-08
Precision: 1.0e-17
match ;     Ez  (x= 10,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.y=0\,z=0, 40, 2) ; -4.60427091128331e-17
Precision: 1.0e-17
match ;     Ez  (x=  0,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.y=0\,z=0, 30, 2) ; -3.0295852646366e-11
Precision: 7.22e-17
match ;     Ez  (x=  0,y=  0,z=-10) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.x=0\,y=0, 20, 2) ; 0.00014444668641218
Precision: 1.0e-17
match ;     Ez  (x=  0,y=  0,z=-10) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.x=0\,y=0, 20, 2) ; 3.001147434765064e-10
Precision: 1.40e-17
match ;     Ez  (x=  0,y=-10,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.x=0\,z=0, 20, 2) ; -0.000280727278916516
Precision: 1.0e-17
match ;     Ez  (x=  0,y=-10,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.x=0\,z=0, 20, 2) ; -3.60911099988195e-10
Precision: 1.0e-17
match ;     Ez  (x=  0,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.x=0\,z=0, 30, 2) ; -3.0295852646366e-11
Precision: 8.89e-17
match ;     By  (x=-10,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.y=0\,z=0, 20, 2) ; 0.0017779289438125102
Precision: 1.0e-17
match ;     By  (x= 10,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.y=0\,z=0, 40, 2) ; 1.9626864454342048e-08
Precision: 1.0e-17
match ;     By  (x=  0,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.y=0\,z=0, 30, 2) ; -8.547444820625581e-10
Precision: 2.06e-14
match ;     By  (x=-10,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.y=0\,z=0, 20, 2) ; -4.1239900699999996e-07
Precision: 1.0e-17
match ;     By  (x= 10,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.y=0\,z=0, 40, 2) ; 0.0
Precision: 1.0e-17
match ;     By  (x=  0,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.y=0\,z=0, 30, 2) ; 4.42159034375378e-13
Precision: 1.0e-17
match ;     Bx  (x=  0,y=-10,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-x\.x=0\,z=0, 20, 2) ; -5.05838765432352e-06
Precision: 1.0e-17
match ;     Bx  (x=  0,y=-10,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-x\.x=0\,z=0, 20, 2) ; 1.7164882607221e-11
Precision: 1.0e-17
match ;     Bx  (x=  0,y= 10,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-x\.x=0\,z=0, 40, 2) ; -1.716488260722095e-11
Precision: 1.0e-17
match ;     Bx  (x=  0,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-x\.x=0\,z=0, 30, 2) ; 0.0
Precision: 1.0e-17
match ;     Bx  (x=  0,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-x\.x=0\,z=0, 30, 2) ; 0.0
Precision: 1.0e-17
match ;     By  (x=  0,y=-10,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.x=0\,z=0, 20, 2) ; -4.3800861697585e-12
Precision: 1.0e-17
match ;     By  (x=  0,y=-10,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.x=0\,z=0, 20, 2) ; -2.10815678727535e-06
Precision: 1.0e-17
match ;     By  (x=  0,y=  0,z=-10) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.x=0\,y=0, 20, 2) ; 5.2673910626928995e-12
Precision: 1.0e-17
match ;     By  (x=  0,y=  0,z=-10) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.x=0\,y=0, 20, 2) ; 4.09713184234935e-06

# Cosinoidal pulse polarized in z-direction passing through medium box, with PML
# If this test is adapted, please also adapt the test below (unpacked) in the same way.
Input      : 03-linear-medium.02-cosinoidal_pulse_td_pml.inp

Precision: 2.29e-12
match ;     Tot. Maxwell energy [step 10]     ; LINEFIELD(Maxwell/td.general/maxwell_energy, 16, 3) ; 59.974704150864696
Precision: 3.86e-12
match ;    Tot. Maxwell energy [step 30]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 36, 3) ; 206.5912392869522
Precision: 5.01e-15
match ;    Ez  (x=-10,y=  0,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.y=0\,z=0, 22, 2) ; -0.100116399325604
Precision: 1.0e-17
match ;    Ez  (x=  0,y=  0,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.y=0\,z=0, 42, 2) ; -3.02773066891615e-11
Precision: 1.00e-16
match ;    Ez  (x=  0,y=  0,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.y=0\,z=0, 42, 2) ; 7.02575139854074e-08
Precision: 1.00e-16
match ;    Ez  (x= 10,y=  0,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.y=0\,z=0, 62, 2) ; -4.540253907358055e-07
Precision: 3.11e-14
match ;    Ez  (x=-10,y=  0,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.y=0\,z=0, 22, 2) ; 0.621326183879285
Precision: 1.0e-17
match ;    Ez  (x=  0,y=-10,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.x=0\,z=0, 22, 2) ; -1.67316357769615e-07
Precision: 1.00e-16
match ;    Ez  (x=  0,y= 10,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.x=0\,z=0, 62, 2) ; -0.000670221560457828
Precision: 1.0e-17
match ;    Ez  (x=  0,y=  0,z=-10) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.x=0\,y=0, 22, 2) ; 1.7391788818322201e-07
Precision: 1.0e-17
match ;    Ez  (x=  0,y=  0,z= 10) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.x=0\,y=0, 62, 2) ; 1.7391788818321998e-07
Precision: 1.50e-11
match ;    Ez  (x=  0,y=  0,z=-10) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.x=0\,y=0, 22, 2) ; -0.000129271572922626
Precision: 1.00e-16
match ;    Ez  (x=  0,y=  0,z= 10) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.x=0\,y=0, 62, 2) ; -0.000129271572922623
Precision: 1.00e-16
match ;    Ex  (x=  0,y=  0,z=-10) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-x\.x=0\,y=0, 22, 2) ; 0.000158478816138979
Precision: 1.00e-16
match ;    Ex  (x=  0,y=  0,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-x\.x=0\,y=0, 42, 2) ; 0.0
Precision: 1.00e-16
match ;    Ex  (x=  0,y=  0,z= 10) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-x\.x=0\,y=0, 62, 2) ; -0.00015847881613898401
Precision: 1.0e-17
match ;    Bx  (x=  0,y=-10,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-x\.x=0\,z=0, 22, 2) ; 3.93893120824365e-10
Precision: 1.0e-17
match ;    Bx  (x=  0,y= 10,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-x\.x=0\,z=0, 62, 2) ; -3.9389312082436604e-10
Precision: 1.0e-17
match ;    Bx  (x=  0,y=-10,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-x\.x=0\,z=0, 22, 2) ; -1.1564757911075001e-06
Precision: 1.0e-17
match ;    Bx  (x=  0,y= 10,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-x\.x=0\,z=0, 62, 2) ; 1.1564757911075402e-06
Precision: 1.0e-17
match ;    Bx  (x=  0,y=  0,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-x\.x=0\,z=0, 42, 2) ; 0.0
Precision: 1.0e-17
match ;    Bx  (x=  0,y=  0,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-x\.x=0\,z=0, 42, 2) ; 0.0
Precision: 3.65e-17
match ;    By  (x=-10,y=  0,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.y=0\,z=0, 22, 2) ; 0.000730584660673994
Precision: 2.27e-16
match ;    By  (x=-10,y=  0,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.y=0\,z=0, 22, 2) ; -0.00453403620460835
Precision: 1.0e-17
match ;    By  (x= 10,y=  0,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.y=0\,z=0, 62, 2) ; 0.0
Precision: 1.0e-17
match ;    By  (x=  0,y=  0,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.y=0\,z=0, 42, 2) ; 4.41888361599622e-13
Precision: 1.0e-17
match ;    By  (x= 10,y=  0,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.y=0\,z=0, 62, 2) ; 3.3131833372202003e-09
Precision: 1.0e-17
match ;    By  (x=  0,y=  0,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.y=0\,z=0, 42, 2) ; -1.02538769593318e-09
Precision: 1.0e-17
match ;    By  (x=  0,y=-10,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.x=0\,z=0, 22, 2) ; 9.433402407064239e-07
Precision: 1.0e-17
match ;    By  (x=  0,y= 10,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.x=0\,z=0, 62, 2) ; 9.43340240706272e-07
Precision: 1.0e-17
match ;    By  (x=  0,y=-10,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.x=0\,z=0, 22, 2) ; -1.2691401426677202e-09
Precision: 1.0e-17
match ;    By  (x=  0,y= 10,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.x=0\,z=0, 62, 2) ; -1.2691401426677251e-09
Precision: 1.0e-17
match ;    By  (x=  0,y=  0,z=-10) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.x=0\,y=0, 22, 2) ; 4.890843004960544e-06
Precision: 1.0e-17
match ;    By  (x=  0,y=  0,z= 10) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.x=0\,y=0, 62, 2) ; 4.8908430049605e-06
Precision: 1.0e-17
match ;    By  (x=  0,y=  0,z=-10) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.x=0\,y=0, 22, 2) ; 1.2209664479519799e-09
Precision: 1.0e-17
match ;    By  (x=  0,y=  0,z= 10) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.x=0\,y=0, 62, 2) ; 1.22096644795196e-09

# Cosinoidal pulse polarized in z-direction passing through medium box, with PML
# This is the same as the second test, but with unpacked states. The values
# to be tested need to stay the same!
Input      : 03-linear-medium.03-cosinoidal_pulse_td_pml_unpacked.inp

Precision: 2.29e-12
match ;     Tot. Maxwell energy [step 10]     ; LINEFIELD(Maxwell/td.general/maxwell_energy, 16, 3) ; 59.974704150864696
Precision: 3.86e-12
match ;    Tot. Maxwell energy [step 30]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 36, 3) ; 206.5912392869522
Precision: 5.01e-15
match ;    Ez  (x=-10,y=  0,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.y=0\,z=0, 22, 2) ; -0.100116399325604
Precision: 1.0e-17
match ;    Ez  (x=  0,y=  0,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.y=0\,z=0, 42, 2) ; -3.02773066891615e-11
Precision: 1.0e-17
match ;    Ez  (x=  0,y=  0,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.y=0\,z=0, 42, 2) ; 7.02575139854074e-08
Precision: 1.0e-17
match ;    Ez  (x= 10,y=  0,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.y=0\,z=0, 62, 2) ; -4.540253907358055e-07
Precision: 3.11e-14
match ;    Ez  (x=-10,y=  0,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.y=0\,z=0, 22, 2) ; 0.621326183879285
Precision: 1.0e-17
match ;    Ez  (x=  0,y=-10,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.x=0\,z=0, 22, 2) ; -1.67316357769615e-07
Precision: 1.0e-17
match ;    Ez  (x=  0,y=  0,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.x=0\,z=0, 42, 2) ; -3.02773066891615e-11
Precision: 3.8e-17
match ;    Ez  (x=  0,y= 10,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.x=0\,z=0, 62, 2) ; -0.000670221560457828
Precision: 1.0e-17
match ;    Ez  (x=  0,y=  0,z=-10) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.x=0\,y=0, 22, 2) ; 1.7391788818322201e-07
Precision: 1.0e-17
match ;    Ez  (x=  0,y=  0,z= 10) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.x=0\,y=0, 62, 2) ; 1.7391788818321998e-07
Precision: 1.0e-17
match ;    Ez  (x=  0,y=  0,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.x=0\,y=0, 42, 2) ; -3.02773066891615e-11
Precision: 4.4e-11
match ;    Ez  (x=  0,y=  0,z=-10) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.x=0\,y=0, 22, 2) ; -0.000129271572922626
Precision: 5.0e-11
match ;    Ez  (x=  0,y=  0,z= 10) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.x=0\,y=0, 62, 2) ; -0.000129271572922623
Precision: 1.20e-17
match ;    Ex  (x=  0,y=  0,z=-10) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-x\.x=0\,y=0, 22, 2) ; 0.000158478816138979
Precision: 1.0e-17
match ;    Ex  (x=  0,y=  0,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-x\.x=0\,y=0, 42, 2) ; 0.0
Precision: 1.65e-17
match ;    Ex  (x=  0,y=  0,z= 10) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-x\.x=0\,y=0, 62, 2) ; -0.00015847881613898401
Precision: 1.0e-17
match ;    Bx  (x=  0,y=-10,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-x\.x=0\,z=0, 22, 2) ; 3.93893120824365e-10
Precision: 1.0e-17
match ;    Bx  (x=  0,y= 10,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-x\.x=0\,z=0, 62, 2) ; -3.9389312082436604e-10
Precision: 1.0e-17
match ;    Bx  (x=  0,y=-10,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-x\.x=0\,z=0, 22, 2) ; -1.1564757911075001e-06
Precision: 1.0e-17
match ;    Bx  (x=  0,y= 10,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-x\.x=0\,z=0, 62, 2) ; 1.1564757911075402e-06
Precision: 1.0e-17
match ;    Bx  (x=  0,y=  0,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-x\.x=0\,z=0, 42, 2) ; 0.0
Precision: 1.0e-17
match ;    Bx  (x=  0,y=  0,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-x\.x=0\,z=0, 42, 2) ; 0.0
Precision: 3.65e-17
match ;    By  (x=-10,y=  0,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.y=0\,z=0, 22, 2) ; 0.000730584660673994
Precision: 2.27e-16
match ;    By  (x=-10,y=  0,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.y=0\,z=0, 22, 2) ; -0.00453403620460835
Precision: 1.0e-17
match ;    By  (x= 10,y=  0,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.y=0\,z=0, 62, 2) ; 0.0
Precision: 1.0e-17
match ;    By  (x=  0,y=  0,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.y=0\,z=0, 42, 2) ; 4.41888361599622e-13
Precision: 1.0e-17
match ;    By  (x= 10,y=  0,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.y=0\,z=0, 62, 2) ; 3.3131833372202003e-09
Precision: 1.0e-17
match ;    By  (x=  0,y=  0,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.y=0\,z=0, 42, 2) ; -1.02538769593318e-09
Precision: 1.0e-17
match ;    By  (x=  0,y=-10,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.x=0\,z=0, 22, 2) ; 9.433402407064239e-07
Precision: 1.0e-17
match ;    By  (x=  0,y= 10,z=  0) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.x=0\,z=0, 62, 2) ; 9.43340240706272e-07
Precision: 1.0e-17
match ;    By  (x=  0,y=-10,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.x=0\,z=0, 22, 2) ; -1.2691401426677202e-09
Precision: 1.0e-17
match ;    By  (x=  0,y= 10,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.x=0\,z=0, 62, 2) ; -1.2691401426677251e-09
Precision: 1.0e-17
match ;    By  (x=  0,y=  0,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.x=0\,z=0, 42, 2) ; 4.41888361599622e-13
Precision: 1.0e-17
match ;    By  (x=  0,y=  0,z=-10) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.x=0\,y=0, 22, 2) ; 4.890843004960544e-06
Precision: 1.0e-17
match ;    By  (x=  0,y=  0,z= 10) [step 30]    ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.x=0\,y=0, 62, 2) ; 4.8908430049605e-06
Precision: 1.0e-17
match ;    By  (x=  0,y=  0,z=-10) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.x=0\,y=0, 22, 2) ; 1.2209664479519799e-09
Precision: 1.0e-17
match ;    By  (x=  0,y=  0,z= 10) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.x=0\,y=0, 62, 2) ; 1.22096644795196e-09
Precision: 1.0e-17
match ;    By  (x=  0,y=  0,z=  0) [step 10]    ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.x=0\,y=0, 42, 2) ; 4.41888361599622e-13
