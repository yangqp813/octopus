# -*- coding: utf-8 mode: shell-script -*-

Test       : Silicon crystal with two k-point shifts
Program    : octopus
TestGroups : long-run, periodic_systems
Enabled    : Yes

Input      : 14-silicon_shifts.01-gs.inp

match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1

match ; Total k-points   ; GREPFIELD(static/info, 'Total number of k-points', 6) ; 35
match ; Space group        ; GREPFIELD(out, 'Space group', 4) ; 227
match ; No. of symmetries  ; GREPFIELD(out, 'symmetries that can be used', 5)  ;  4

Precision: 3.97e-07
match ;   Total energy         ; GREPFIELD(static/info, 'Total       =', 3) ; -7.93790937
Precision: 3.93e-06
match ;   Ion-ion energy       ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -7.8578008
Precision: 1.31e-07
match ;   Eigenvalues sum      ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -0.26116702
Precision: 1.00e-07
match ;   Hartree energy       ; GREPFIELD(static/info, 'Hartree     =', 3) ; 0.551596725
Precision: 1.02e-07
match ;   Exchange energy      ; GREPFIELD(static/info, 'Exchange    =', 3) ; -2.03516595
Precision: 1.88e-07
match ;   Correlation energy   ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.37507423
Precision: 1.55e-07
match ;   Kinetic energy       ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 3.09124189
Precision: 3.00e-07
match ;  External energy       ; GREPFIELD(static/info, 'External    =', 3) ; -1.31270696

Precision: 2.50e-03
match ;   k-point 1 (x)   ; GREPFIELD(static/info, '#k =       1', 7) ; 0.25
match ;   k-point 1 (y)   ; GREPFIELD(static/info, '#k =       1', 8) ; 0.0
match ;   k-point 1 (z)   ; GREPFIELD(static/info, '#k =       1', 9) ; 0.0
Precision: 2.58e-15
match ;   Eigenvalue  1   ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -0.258277
Precision: 4.27e-05
match ;   Eigenvalue  2   ; GREPFIELD(static/info, '#k =       1', 3, 2) ; 0.008546
Precision: 6.27e-07
match ;   Eigenvalue  4   ; GREPFIELD(static/info, '#k =       1', 3, 4) ; 0.125325
Precision: 1.14e-05
match ;   Eigenvalue  5   ; GREPFIELD(static/info, '#k =       1', 3, 5) ; 0.227272

Precision: 6.27e-07
match ;   DOS E Fermi      ; LINEFIELD(static/total-dos-efermi.dat, 2, 1) ; 0.125325
Precision: 2.27e-05
match ;   DOS energy 2     ; LINEFIELD(static/total-dos.dat, 2, 1) ; -0.454059
Precision: 4.99e-05
match ;   DOS value  2     ; LINEFIELD(static/total-dos.dat, 2, 2) ; 0.0997940
Precision: 7.59e-05
match ;   DOS energy 442   ; LINEFIELD(static/total-dos.dat, 442, 1) ; 0.58174
Precision: 8.46e-04
match ;   DOS value  442   ; LINEFIELD(static/total-dos.dat, 442, 2) ; 0.79389

#Checking some TD quantities
Input      : 14-silicon_shifts.02-td.inp
Precision: 7.88e-08
match ;        Total current [step  100]     ; LINEFIELD(td.general/total_current, -1, 3) ; 0.012262589094607472
Precision: 3.65e-08
match ;         Projections [step 100]         ; LINEFIELD(td.general/projections, -1, 3) ; 0.9400425513188146
Precision: 1.01e-07
match ;         Projections [step 100]         ; LINEFIELD(td.general/projections, -1, 4) ; -0.3410433695125176
Precision: 1.56e-10
match ;           Stress (11) [step 100]            ; GREPFIELD(output_iter/td.0000100/stress, 'Total stress tensor', 2, 2) ; 8.56759127e-05
Precision: 2.13e-11
match ;           Stress (11) [step 0]              ; GREPFIELD(output_iter/td.0000000/stress, 'Total stress tensor', 2, 2) ; -0.0005966737806
Precision: 1.00e-04
match ;           Stress (12) [step 0]              ; GREPFIELD(output_iter/td.0000000/stress, 'Total stress tensor', 3, 2) ; 0.0
Precision: 1.00e-04
match ;           Stress (13) [step 0]              ; GREPFIELD(output_iter/td.0000000/stress, 'Total stress tensor', 4, 2) ; 0.0
Precision: 1.00e-04
match ;           Stress (21) [step 0]              ; GREPFIELD(output_iter/td.0000000/stress, 'Total stress tensor', 2, 3) ; 0.0
Precision: 2.30e-11
match ;           Stress (22) [step 0]              ; GREPFIELD(output_iter/td.0000000/stress, 'Total stress tensor', 3, 3) ; -0.0005980850222
Precision: 2.68e-12
match ;           Stress (23) [step 0]              ; GREPFIELD(output_iter/td.0000000/stress, 'Total stress tensor', 4, 3) ; 6.403011026e-06
Precision: 1.00e-04
match ;           Stress (31) [step 0]              ; GREPFIELD(output_iter/td.0000000/stress, 'Total stress tensor', 2, 4) ; 0.0
Precision: 2.68e-12
match ;           Stress (32) [step 0]              ; GREPFIELD(output_iter/td.0000000/stress, 'Total stress tensor', 3, 4) ; 6.403011026e-06
Precision: 2.30e-11
match ;           Stress (33) [step 0]              ; GREPFIELD(output_iter/td.0000000/stress, 'Total stress tensor', 4, 4) ; -0.0005980850222
Precision: 1.00e-04
match ;           Stress (12) [step 100]            ; GREPFIELD(output_iter/td.0000100/stress, 'Total stress tensor', 3, 2) ; 0.0
Precision: 1.00e-04
match ;           Stress (13) [step 100]            ; GREPFIELD(output_iter/td.0000100/stress, 'Total stress tensor', 4, 2) ; 0.0
Precision: 1.00e-04
match ;           Stress (21) [step 100]            ; GREPFIELD(output_iter/td.0000100/stress, 'Total stress tensor', 2, 3) ; 0.0
Precision: 1.71e-10
match ;           Stress (22) [step 100]            ; GREPFIELD(output_iter/td.0000100/stress, 'Total stress tensor', 3, 3) ; 8.428695685999999e-05
Precision: 9.19e-11
match ;           Stress (23) [step 100]            ; GREPFIELD(output_iter/td.0000100/stress, 'Total stress tensor', 4, 3) ; 6.471860535000001e-06
Precision: 1.00e-04
match ;           Stress (31) [step 100]            ; GREPFIELD(output_iter/td.0000100/stress, 'Total stress tensor', 2, 4) ; 0.0
Precision: 9.19e-11
match ;           Stress (32) [step 100]            ; GREPFIELD(output_iter/td.0000100/stress, 'Total stress tensor', 3, 4) ; 6.471860535000001e-06
Precision: 1.71e-10
match ;         Stress (33) [step 100]          ; GREPFIELD(output_iter/td.0000100/stress, 'Total stress tensor', 4, 4) ; 8.428695685999999e-05
Precision: 1.05e-9
match ;          Number of excited electrons [step  100]          ; LINEFIELD(td.general/n_ex, -1, 3) ; 6.185436383068788e-05

#Results here are copy pasted from the previous test as we are just changing the parallelization scheme
#Only n_ex is not copied, as this is not implemented at the moment
Input      : 14-silicon_shifts.03-td_parstates.inp

Precision: 3.23e-09
match ;      Number of excited electrons [step  100]       ; LINEFIELD(td.general/n_ex, -1, 3) ; 6.185219895782357e-05
Precision: 3.14e-04
match ;      Projections [step 100]      ; LINEFIELD(td.general/projections, -1, 3) ; 0.9400425766754038
Precision: 5.83e-04
match ;      Projections [step 100]      ; LINEFIELD(td.general/projections, -1, 4) ; -0.341043299647169
Precision: 2.68e-02
match ;     Total current [step  100]  ; LINEFIELD(td.general/total_current, -1, 3) ; 0.01226255951247051


Input      : 14-silicon_shifts.04-delayed_kick.inp

Precision: 9.94e-06
match ;     Total current [step  100]     ; LINEFIELD(td.general/gauge_field, -1, 3) ; 1.7208454564477198

Util : oct-dielectric-function
Input      : 14-silicon_shifts.05-dielectric_function.inp

Precision: 1.00e-04
match ;   epsilon file  energy 0   ; LINEFIELD(td.general/dielectric_function, 2, 1) ; 0.0
Precision: 1.23e-04
match ;   Re epsilon xx energy 0   ; LINEFIELD(td.general/dielectric_function, 2, 2) ; 2.46781
Precision: 1.00e-04
match ;   Im epsilon xx energy 0   ; LINEFIELD(td.general/dielectric_function, 2, 3) ; 0.0
Precision: 5.00e-15
match ;   Re epsilon yy energy 0   ; LINEFIELD(td.general/dielectric_function, 2, 4) ; 0.0
Precision: 1.00e-04
match ;   Im epsilon yy energy 0   ; LINEFIELD(td.general/dielectric_function, 2, 5) ; 0.0
Precision: 5.00e-15
match ;   Re epsilon zz energy 0   ; LINEFIELD(td.general/dielectric_function, 2, 6) ; 0.0
Precision: 1.00e-04
match ;   Im epsilon zz energy 0   ; LINEFIELD(td.general/dielectric_function, 2, 7) ; 0.0
