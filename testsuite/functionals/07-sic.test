# -*- coding: utf-8 mode: shell-script -*-

Test       : LDA+SIC/OEP-KLI Functional
Program    : octopus
TestGroups : long-run, functionals
Enabled    : Yes

# This test performs the calculation of an argon atom making
# use of the local-density approximation (LDA) plus the self-interaction
# correction (SIC), treating this latter term with the Krieger-Li-Iafrate
# approximation (KLI) to the optimized effective potential method (OEP).
# This is the functional used in:
# [X.-M. Tong and S.-I Chu, Phys. Rev. A 55, 3406 (1997)],
# to the best of my knowledge for the first time.
#
# The calculation reproduces the result given for Ar in Table IV of this paper.
# It is the value of the energy of the HOMO (a.k.a. ionization potential): 0.549Ha.
#
# The number is missed by 0.006Ha = 0.16eV. The reason is probably that we
# use a LDA pseudopotential, whereas the results in the paper are all-electron.

Input     : 07-sic.01-gs.inp

match ;  SCF convergence     ; GREPCOUNT(static/info, 'SCF converged') ; 1.0
Precision: 2.94e-04
match ;        Total energy              ; GREPFIELD(static/info, 'Total       =', 3) ; -20.79684332
Precision: 1.00e-04
match ;        Ion-ion energy            ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; 0.0
Precision: 1.35e-04
match ;        Eigenvalues sum           ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -5.3575315
Precision: 3.34e-04
match ;        Hartree energy            ; GREPFIELD(static/info, 'Hartree     =', 3) ; 18.03583342
Precision: 4.37e-04
match ;        Int[n*v_xc]               ; GREPFIELD(static/info, 'Int\[n\*v_xc\] =', 3) ; -5.879242529999999
Precision: 2.54e-04
match ;        Exchange energy           ; GREPFIELD(static/info, 'Exchange    =', 3) ; -3.2827136
Precision: 1.00e-04
match ;        Correlation energy        ; GREPFIELD(static/info, 'Correlation =', 3) ; 0.0
Precision: 1.47e-04
match ;        Kinetic energy            ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 7.8157814199999995
Precision: 4.57e-04
match ;        External energy           ; GREPFIELD(static/info, 'External    =', 3) ; -43.365799960000004
Precision: 1.43e-05
match ;       Eigenvalue 1             ; GREPFIELD(static/info, '1   --', 3) ; -1.0516159999999999
Precision: 2.71e-05
match ;       Eigenvalue 2             ; GREPFIELD(static/info, '2   --', 3) ; -0.542404
Precision: 3.30e-05
match ;       Eigenvalue 3             ; GREPFIELD(static/info, '3   --', 3) ; -0.542371
Precision: 2.71e-05
match ;       Eigenvalue 4             ; GREPFIELD(static/info, '4   --', 3) ; -0.542366

#The following test only runs in serial at the moment
Processors : 1
Input     : 07-sic.02-scdm.inp

match ; SCF convergence    ; GREPCOUNT(static/info, 'SCF converged') ; 1

Precision: 4.39e-05
match ;     Total energy           ; GREPFIELD(static/info, 'Total       =', 3) ; -20.9280911
Precision: 1.00e-04
match ;     Ion-ion energy         ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; 0.0
Precision: 1.87e-05
match ;     Eigenvalues sum        ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -5.4915373999999995
Precision: 9.09e-05
match ;     Hartree energy         ; GREPFIELD(static/info, 'Hartree     =', 3) ; 18.18233
Precision: 1.78e-05
match ;     Int[n*v_xc]            ; GREPFIELD(static/info, 'Int\[n\*v_xc\] =', 3) ; -6.19150232
Precision: 9.01e-06
match ;     Exchange energy        ; GREPFIELD(static/info, 'Exchange    =', 3) ; -3.44572283
Precision: 1.00e-04
match ;     Correlation energy     ; GREPFIELD(static/info, 'Correlation =', 3) ; 0.0
Precision: 1.02e-04
match ;     Kinetic energy         ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 7.92544494
Precision: 1.55e-04
match ;     External energy        ; GREPFIELD(static/info, 'External    =', 3) ; -43.59014637

Precision: 5.50e-06
match ;     Eigenvalue 1           ; GREPFIELD(static/info, '1   --', 3) ; -1.070268
Precision: 2.80e-05
match ;     Eigenvalue 2           ; GREPFIELD(static/info, '2   --', 3) ; -0.560715
Precision: 2.80e-05
match ;     Eigenvalue 3           ; GREPFIELD(static/info, '3   --', 3) ; -0.560317
Precision: 2.77e-04
match ;     Eigenvalue 4           ; GREPFIELD(static/info, '4   --', 3) ; -0.55447
