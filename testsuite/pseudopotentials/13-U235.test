# -*- coding: utf-8 mode: shell-script -*-

Test       : Uranium 235
Program    : octopus
TestGroups : long-run, pseudopotentials
Enabled    : Yes

#This tests whether we can modify a pseudopotential from a set without
#having to specify the pseudopotential file or the set.

Input: 13-U235.01-gs.inp
match ; SCF convergence    ;     GREPCOUNT(static/info, 'SCF converged') ; 1


Precision: 2.67e-07
match ;   Total energy        ; GREPFIELD(static/info, 'Total       =', 3) ; -53.32624942
Precision: 1.00e-04
match ;   Ion-ion energy      ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; 0.0
Precision: 9.50e-08
match ;   Eigenvalues sum     ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -19.00363437
Precision: 1.80e-07
match ;   Hartree energy      ; GREPFIELD(static/info, 'Hartree     =', 3) ; 35.96498013
Precision: 3.56e-07
match ;   Int[n*v_xc]         ; GREPFIELD(static/info, 'Int\[n\*v_xc\] =', 3) ; -7.11202717
Precision: 2.53e-07
match ;   Exchange energy     ; GREPFIELD(static/info, 'Exchange    =', 3) ; -5.05064697
Precision: 2.10e-07
match ;   Correlation energy  ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.41901511
Precision: 2.17e-13
match ;   Kinetic energy      ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 21.704406130000002
Precision: 5.28e-08
match ;   External energy     ; GREPFIELD(static/info, 'External    =', 3) ; -105.52597396
Precision: 3.53e-05
match ;   Eigenvalue 1        ; GREPFIELD(static/info, '  1   --', 3) ; -7.051018999999999
Precision: 3.28e-05
match ;   Eigenvalue 1        ; GREPFIELD(static/info, '  2   --', 3) ; -0.655243
Precision: 3.28e-05
match ;   Eigenvalue 1        ; GREPFIELD(static/info, '  3   --', 3) ; -0.655243
Precision: 3.28e-05
match ;   Eigenvalue 1        ; GREPFIELD(static/info, '  4   --', 3) ; -0.655242
Precision: 1.80e-05
match ;   Eigenvalue 1        ; GREPFIELD(static/info, '  5   --', 3) ; -0.360169
Precision: 6.25e-06
match ;   Eigenvalue 1        ; GREPFIELD(static/info, '  6   --', 3) ; -0.124902
Precision: 2.14e-01
match ;   mass                ; GREPFIELD(debug/geometry/U/info, 'mass ', 3) ; 428458.87
Precision: 9.20e-01
match ;   z                   ; GREPFIELD(debug/geometry/U/pseudo-info, 'z ', 3) ; 92.0
Precision: 1.20e-01
match ;   z valence           ; GREPFIELD(debug/geometry/U/pseudo-info, 'zval', 3) ; 12.0
Precision: 3.00e-02
match ;   l max               ; GREPFIELD(debug/geometry/U/pseudo-info, 'lmax', 3) ; 3.0
Precision: 3.00e-02
match ;   l loc               ; GREPFIELD(debug/geometry/U/pseudo-info, 'lloc', 3) ; 3.0
Precision: 1.00e-02
match ;   kbc                 ; GREPFIELD(debug/geometry/U/pseudo-info, 'kbc', 3) ; 1.0
Precision: 1.74e-01
match ;   rcmax               ; GREPFIELD(debug/geometry/U/pseudo-info, 'rcmax', 3) ; 3.48
Precision: 1.00e-04
match ;   potential r       2 ; LINEFIELD(debug/geometry/U/local, 2, 1) ; 0.01
Precision: 3.61e-12
match ;   potential value   2 ; LINEFIELD(debug/geometry/U/local, 2, 2) ; -7.21438673e-05
Precision: 4.90e-03
match ;   potential r      50 ; LINEFIELD(debug/geometry/U/local, 50, 1) ; 0.49
Precision: 2.39e-07
match ;   potential value  50 ; LINEFIELD(debug/geometry/U/local, 50, 2) ; -4.78360289
Precision: 9.90e-03
match ;   potential r     100 ; LINEFIELD(debug/geometry/U/local, 100, 1) ; 0.99
Precision: 2.49e-07
match ;   potential value 100 ; LINEFIELD(debug/geometry/U/local, 100, 2) ; -4.98136317
Precision: 9.95e-02
match ;   potential r     200 ; LINEFIELD(debug/geometry/U/local, 200, 1) ; 1.99
Precision: 1.03e-07
match ;   potential value 200 ; LINEFIELD(debug/geometry/U/local, 200, 2) ; -0.20617752
Precision: 1.50e-01
match ;   potential r     300 ; LINEFIELD(debug/geometry/U/local, 300, 1) ; 2.99
Precision: 1.89e-11
match ;   potential value 300 ; LINEFIELD(debug/geometry/U/local, 300, 2) ; -0.00037772664200000003
Precision: 2.00e-01
match ;   potential r     400 ; LINEFIELD(debug/geometry/U/local, 400, 1) ; 3.99
Precision: 1.46e-10
match ;   potential value 400 ; LINEFIELD(debug/geometry/U/local, 400, 2) ; 0.00292976529
Precision: 1.00e-04
match ;   projector r       2 ; LINEFIELD(debug/geometry/U/nonlocal, 2, 1) ; 0.01
Precision: 4.44e-08
match ;   projector value   2 ; LINEFIELD(debug/geometry/U/nonlocal, 2, 2) ; 0.888820595
Precision: 2.95e-09
match ;   projector value   2 ; LINEFIELD(debug/geometry/U/nonlocal, 2, 3) ; 0.0589928099
Precision: 3.86e-11
match ;   projector value   2 ; LINEFIELD(debug/geometry/U/nonlocal, 2, 4) ; 0.000771224612
Precision: 1.00e-04
match ;   projector value   2 ; LINEFIELD(debug/geometry/U/nonlocal, 2, 5) ; 0.0
Precision: 4.90e-03
match ;   projector r      50 ; LINEFIELD(debug/geometry/U/nonlocal, 50, 1) ; 0.49
Precision: 4.93e-08
match ;   projector value  50 ; LINEFIELD(debug/geometry/U/nonlocal, 50, 2) ; 0.985907632
Precision: 9.83e-08
match ;   projector value  50 ; LINEFIELD(debug/geometry/U/nonlocal, 50, 3) ; 1.96601794
Precision: 5.94e-08
match ;   projector value  50 ; LINEFIELD(debug/geometry/U/nonlocal, 50, 4) ; 1.18744341
Precision: 1.00e-04
match ;   projector value  50 ; LINEFIELD(debug/geometry/U/nonlocal, 50, 5) ; 0.0
Precision: 9.90e-03
match ;   projector r     100 ; LINEFIELD(debug/geometry/U/nonlocal, 100, 1) ; 0.99
Precision: 4.03e-08
match ;   projector value 100 ; LINEFIELD(debug/geometry/U/nonlocal, 100, 2) ; 0.805099467
Precision: 5.14e-08
match ;   projector value 100 ; LINEFIELD(debug/geometry/U/nonlocal, 100, 3) ; 1.02830098
Precision: 5.99e-08
match ;   projector value 100 ; LINEFIELD(debug/geometry/U/nonlocal, 100, 4) ; 1.19785726
Precision: 1.00e-04
match ;   projector value 100 ; LINEFIELD(debug/geometry/U/nonlocal, 100, 5) ; 0.0
Precision: 9.95e-02
match ;   projector r     200 ; LINEFIELD(debug/geometry/U/nonlocal, 200, 1) ; 1.99
Precision: 1.61e-08
match ;   projector value 200 ; LINEFIELD(debug/geometry/U/nonlocal, 200, 2) ; 0.321069765
Precision: 3.40e-11
match ;   projector value 200 ; LINEFIELD(debug/geometry/U/nonlocal, 200, 3) ; 0.000680911233
Precision: 2.40e-09
match ;   projector value 200 ; LINEFIELD(debug/geometry/U/nonlocal, 200, 4) ; 0.0480161642
Precision: 1.00e-04
match ;   projector value 200 ; LINEFIELD(debug/geometry/U/nonlocal, 200, 5) ; 0.0
Precision: 1.50e-01
match ;   projector r     300 ; LINEFIELD(debug/geometry/U/nonlocal, 300, 1) ; 2.99
Precision: 8.35e-16
match ;   projector value 300 ; LINEFIELD(debug/geometry/U/nonlocal, 300, 2) ; 0.0166972582
Precision: 2.65e-17
match ;   projector value 300 ; LINEFIELD(debug/geometry/U/nonlocal, 300, 3) ; -0.000529149587
Precision: 4.63e-11
match ;   projector value 300 ; LINEFIELD(debug/geometry/U/nonlocal, 300, 4) ; -0.000925030838
Precision: 1.00e-04
match ;   projector value 300 ; LINEFIELD(debug/geometry/U/nonlocal, 300, 5) ; 0.0
Precision: 2.00e-01
match ;   projector r     400 ; LINEFIELD(debug/geometry/U/nonlocal, 400, 1) ; 3.99
Precision: 4.02e-13
match ;   projector value 400 ; LINEFIELD(debug/geometry/U/nonlocal, 400, 2) ; 8.04400661e-06
Precision: 6.13e-18
match ;   projector value 400 ; LINEFIELD(debug/geometry/U/nonlocal, 400, 3) ; -0.000122666622
Precision: 2.73e-12
match ;   projector value 400 ; LINEFIELD(debug/geometry/U/nonlocal, 400, 4) ; -5.45077021e-05
Precision: 1.00e-04
match ;   projector value 400 ; LINEFIELD(debug/geometry/U/nonlocal, 400, 5) ; 0.0