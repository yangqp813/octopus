#[==============================================================================================[
#                                 etsf-io compatibility wrapper                                 #
]==============================================================================================]

#[===[.md
# Findetsf-io

etsf-io compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

# Early exit if module already found and defined
# These files do not contain components, so early-exiting is acceptable
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND AND TARGET etsf_io::etsf_io)
    return()
endif ()

list(APPEND CMAKE_MESSAGE_CONTEXT Findetsf-io)
include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        HAVE_FALLBACK
        NAMES etsf-io
        PKG_MODULE_NAMES etsf-io)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(etsf_io::etsf_io ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
endif ()
# Temporary workarounds for non-packages
if (NOT ${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    find_library(etsf_io_LIBRARY
            NAMES etsf_io
    )
    mark_as_advanced(etsf_io_LIBRARY)
    find_library(etsf_io_utils_LIBRARY
            NAMES etsf_io_utils
    )
    mark_as_advanced(etsf_io_utils_LIBRARY)
    find_path(etsf_io_INCLUDE_DIR
            NAMES etsf_io.mod
            PATH_SUFFIXES etsf etsf_io
    )
    mark_as_advanced(etsf_io_INCLUDE_DIR)
    find_package_handle_standard_args(${CMAKE_FIND_PACKAGE_NAME}
            REQUIRED_VARS etsf_io_LIBRARY etsf_io_utils_LIBRARY etsf_io_INCLUDE_DIR
    )
    if(${CMAKE_FIND_PACKAGE_NAME}_FOUND)
        set(${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS ${etsf_io_INCLUDE_DIR})
        set(${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES ${etsf_io_LIBRARY} ${etsf_io_utils_LIBRARY})
        add_library(etsf_io::etsf_io UNKNOWN IMPORTED)
        set_target_properties(etsf_io::etsf_io PROPERTIES
                IMPORTED_LOCATION ${etsf_io_LIBRARY}
                IMPORTED_LINK_INTERFACE_LANGUAGES Fortran
        )
        target_link_libraries(etsf_io::etsf_io INTERFACE ${${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES})
        target_include_directories(etsf_io::etsf_io INTERFACE ${${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS})
    endif()
endif ()
if(${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    set(HAVE_ETSF_IO 1)
    set(PACKAGE_STRING "Octopus")
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
        URL https://github.com/ElectronicStructureLibrary/libetsf_io
        DESCRIPTION "Library to read and write in conformance with the ETSF file format"
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
