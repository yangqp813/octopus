# TODO: Remove non-namespaced options when upstream accepts it
set(ENABLE_FORTRAN ON CACHE BOOL "Octopus: Overloaded" FORCE)
set(BUILD_TESTING OFF CACHE BOOL "Octopus: Overloaded" FORCE)
set(LIBXC_ENABLE_FORTRAN ON CACHE BOOL "Octopus: Overloaded" FORCE)
set(LIBXC_BUILD_TESTING OFF CACHE BOOL "Octopus: Overloaded" FORCE)
set(LIBXC_IGNORE_DEPRECATED ON CACHE BOOL "Octopus: Overloaded" FORCE)
