list(APPEND CMAKE_MESSAGE_CONTEXT GPUawareMPI)
find_package(MPI QUIET COMPONENTS C)
try_compile(gpu_aware_mpi_compile ${CMAKE_CURRENT_BINARY_DIR}/CMakeFiles/CMakeTmp
	SOURCES ${CMAKE_CURRENT_LIST_DIR}/test_gpu_aware_mpi.c
        COMPILE_DEFINITIONS -DHAVE_HIP=${HAVE_HIP} -DHAVE_CUDA=${HAVE_CUDA}
	LINK_LIBRARIES MPI::MPI_C
)
if (gpu_aware_mpi_compile)
	set(HAVE_CUDA_MPI 1)
else ()
	message(STATUS "Failed to compile test_gpu_aware_mpi.c")
endif ()
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
