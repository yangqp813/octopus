#[==============================================================================================[
#                                 Spglib compatibility wrapper                                 #
]==============================================================================================]

#[===[.md
# FindSpglib

Spglib compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

list(APPEND CMAKE_MESSAGE_CONTEXT FindSpglib)
include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        NAMES Spglib spglib
        PKG_MODULE_NAMES spglib_f08)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(Spglib::fortran ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
elseif (NOT TARGET Spglib::fortran AND TARGET spglib_f08)
    add_library(Spglib::fortran ALIAS spglib_f08)
endif ()

set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
        URL https://github.com/spglib/spglib
        DESCRIPTION "C library for finding and handling crystal symmetries "
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
