#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

rm *.log *.txt *.png *.dat


octopus > log
gnuplot plot1.gnu

./get_current.sh
gnuplot plot2.gnu

gnuplot plot3.gnu
gnuplot plot4.gnu

cp inp *.txt *.gnu tutorial.sh get_current.sh $OCTOPUS_TOP/doc/tutorials/maxwell/3.external-current/2.gaussian_current_pulse_with_pml

cp *.png $OCTOPUS_TOP/doc/html/images/Maxwell/
