#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

rm *.log *.txt


octopus > log
gnuplot plot1.gnu
gnuplot plot2.gnu


cp inp *.txt *.gnu tutorial.sh $OCTOPUS_TOP/doc/tutorials//maxwell/4.drude-medium/

cp *.png $OCTOPUS_TOP/doc/html/images/Maxwell/
