set pm3d
set view map
set palette defined (-0.1 "blue", 0 "white", 0.1 "red")
set term png size 1000,500

unset surface
unset key

set output 'tutorial_02_run_electric_field_contour.png'

set xlabel 'x-direction'
set ylabel 'y-direction'
set cbrange [-1.0:1.0]

set multiplot

set origin 0.025,0
set size 0.45,0.9
set size square
set title 'Electric field E_z (t=0.084263 au)'
sp [-10:10][-10:10][-1.0:1.0] 'Maxwell/output_iter/td.0000040/e_field-z.z=0' u 1:2:3

set origin 0.525,0
set size 0.45,0.9
set size square
set title 'Electric field E_z (t=0.126394 au)'
sp [-10:10][-10:10][-1.0:1.0] 'Maxwell/output_iter/td.0000060/e_field-z.z=0' u 1:2:3

unset multiplot

