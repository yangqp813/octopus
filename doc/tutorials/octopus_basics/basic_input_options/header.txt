    <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
                                ___
                             .-'   `'.
                            /         \
                            |         ;
                            |         |           ___.--,
                   _.._     |0) ~ (0) |    _.---'`__.-( (_.
            __.--'`_.. '.__.\    '--. \_.-' ,.--'`     `""`
           ( ,.--'`   ',__ /./;   ;, '.__.'`    __
           _`) )  .---.__.' / |   |\   \__..--""  """--.,_
          `---' .'.''-._.-'`_./  /\ '.  \ _.-~~~````~~~-._`-.__.'
                | |  .' _.-' |  |  \  \  '.               `~---`
                 \ \/ .'     \  \   '. '-._)
                  \/ /        \  \    `=.__`~-.
             jgs  / /\         `) )    / / `"".`\
            , _.-'.'\ \        / /    ( (     / /
             `--~`   ) )    .-'.'      '.'.  | (
                    (/`    ( (`          ) )  '-;
                     `      '-;         (-'

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2, or (at your option)
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA

    <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

                           Running octopus

Version                : mercatoris
Commit                 : afbc1c5f59003f4d895e759f8da004e124cf89bd
Configuration time     : Thu Feb  1 14:29:48 CET 2024
Configuration options  :  mpi sse2 avx libxc5 libxc_fxc libxc_kxc
Optional libraries     : berkeleygw cgal ELPA etsf_io psolver libvdwxc metis netcdf nfft parmetis scalapack sparskit nlopt
Architecture           : x86_64
C compiler             : mpicc (gcc)
C compiler flags       : -g -Wall -O2 -march=native -Wextra -pedantic -ftest-coverage -fprofile-arcs
C++ compiler           : mpicxx (g++)
C++ compiler flags     : -g -Wall -O2 -march=native -Wextra -pedantic -ftest-coverage -fprofile-arcs
Fortran compiler       : mpifort (gfortran) (GCC version 11.3.0)
Fortran compiler flags : -g -fno-var-tracking-assignments -Wall -Wno-maybe-uninitialized -Wno-surprising -O2 -march=native -fbacktrace -fcheck=all -fbounds-check -finit-real=snan -ffpe-trap=zero,invalid -ftest-coverage -fprofile-arcs

             The octopus is swimming in poppyseed (Linux)


            Calculation started on 2024/02/02 at 12:48:28
