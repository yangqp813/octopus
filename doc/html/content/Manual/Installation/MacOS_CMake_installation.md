---
title: "MacOS CMake Installation with Homebrew"
section: "Manual"
weight: 6
---

Since the introduction of the `CMake` build system for Octopus, it is also possible to use this on MacOS.
Here we give the instructions, using `Homebrew`. `MacPorts` might work, but is untested and currently unsupported.

### Preparing the homebrew environment

#### Installing homebrew

You can install `homebrew` by following the instructions on [HomeBrew](https://brew.sh/).

We recommend adding the following line to your `.zprofile`:
```
eval "$(/opt/homebrew/bin/brew shellenv)"
```
Otherwise, you need to execute this command before you can use any brew-installed packages.

#### Installing required packages

For a `CMake` installation of Octopus, you need a number of packages on your system. First, you need to install
`CMake` itself, as well as the compiler, the build system and `pkg-config`. You can do this with
```shell
brew install cmake ninja pkg-config gcc@13
```

Octopus has some required dependencies, some of which can be automatically installed by `CMake`
but a few need to be installed with `brew`. These are `fftw`, `gsl` and for parallel builds also `metis`.
They can be installed with:
```shell
brew install fftw openblas metis
brew install --cc=gcc13 open-mpi
brew install --cc=gcc13 gsl
```
{{<notice warning>}}
Note that we **do not** install `libxc` from brew. While this works with sequential builds, MPI builds have a problem,
which requires building `libxc` itself with `CMake`
{{</notice>}}


### Configuring and compiling with CMake

With this environment in place, you can configure Octopus simply by
```shell
cmake --preset mac-brew-gcc13 --install-prefix=<dir>
```

This configures a serial build of Octopus. If you want to enable MPI, you need to add the option
```
-DOCTOPUS_MPI=ON
```
to the above command.

{{<notice note>}}
LibXC can be fetched and built from source with the option:
```shell
-DCMAKE_DISABLE_FIND_PAKCAGE_Libxc=ON
```
Including this option will download `libxc` and configure it with default values, which disables the 3rd order kernels.
These are needed for a subset of Octopus features, such as some Sternheimer or Casida calculations (and their corresponding 
tests). In case you need these kernels, add `-DLIBXC_DISABLE_KXC=OFF` to the command line.
{{</notice>}}

{{<notice info>}}
If you want to check the result of the configuration, you can inspect the file `cmake_build_macos/CMakeCache.txt`,
which contains more details on installed packages, etc. This can also be inspected with `ccmake`:
```shell
ccmake ./cmake-build-mac
```
{{</notice>}}

You can now compile the code using:
```shell
cmake --build --preset mac-brew-gcc13
```
The first compilation might take a while, as it also will compile remaining dependencies, such as `libxc`.

### Testing the compiled code

To test the code, you can invoke:
```shell
ctest --preset mac-brew-gcc13
```
This will run a subset of the tests, which should complete in about 20 minutes (depending on your machine).

{{<notice note>}}
At the time of writing, it is expected that some tests will fail, as the results of some sensitive tests fall
slightly outside the set tolerances.

For additional troubleshooting, please reference directly to the cmake README in `OCTROOT/cmake/README.md`.
{{</notice>}}

### Compilation Issues

`SPARSKIT` on Mac can accidentally link to `/Library/Developer/CommandLineTools/SDKs/.../usr/lib/libskit.tbd`.
Please disable "SPARSKIT" if that happens via `CMAKE_DISABLE_FIND_PACKAGE_SPARSKIT=On`, or point to a user-installed
version.

#### Homebrew with XCode 14.x.x

XCode 14.x.x has a [known bug](https://github.com/orgs/Homebrew/discussions/3659) in its linker, which causes a failure 
in the linking of binaries at the end of the cmake build:

```shell
A linker snapshot was created at:
	/tmp/oct-casida_spectrum-2024-03-18-130900.ld-snapshot
ld: Assertion failed: (_file->_atomsArrayCount == computedAtomCount && "more atoms allocated than expected"), function parse, file macho_relocatable_file.cpp, line 2061.
collect2: error: ld returned 1 exit status
```

If you are on an older version of Mac OS (12 or 13), and using Homebrew as your package manager, you may experience
this problem. You may either:

* Upgrade to the latest Xcode, which requires an OS upgrade
* Switch to a newer CLT SDK: `sudo xcode-select --switch /Library/Developer/CommandLineTools`
